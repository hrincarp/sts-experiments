#!/usr/bin/env bash
BASE_PATH=$(pwd)/..

DEVICE=cpu
#DEVICE=gpu

SRC_PATH=$BASE_PATH/src


export PYTHONPATH=$PYTHONPATH:$SRC_PATH
export THEANO_FLAGS=floatX=float32,device=$DEVICE