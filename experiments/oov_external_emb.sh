#!/usr/bin/env bash

source config.sh

OUT_DIR=$(pwd)/output/oov_external_emb_2/
mkdir -p $OUT_DIR

EXP=experiments/oov_experiment.py

#PARAMS="--dataset_root $BASE_PATH/data/semeval-sts/all --train 2015.val.tsv --valid 2015.val.tsv --test 2015.test.tsv --do_not_save"
PARAMS="--dataset_root $BASE_PATH/data/semeval-sts/all --train 2015.train.tsv --valid 2015.val.tsv --test 2015.test.tsv --do_not_save"


HYPERPARAMETERS="--name baselinefix -t_oov emb-fix -rl True -vl -1 -n_oov 5 -lc True -st False -tk True -rs True"
LOG_FILE=${HYPERPARAMETERS////_}}
LOG_FILE=${LOG_FILE// /_}}.log

python -u $SRC_PATH/$EXP $PARAMS $HYPERPARAMETERS| tee -i $OUT_DIR/$LOG_FILE



HYPERPARAMETERS="--name baseline -t_oov oov -vl -1 -n_oov 5 -lc True -st False -tk True -rs True"
LOG_FILE=${HYPERPARAMETERS////_}}
LOG_FILE=${LOG_FILE// /_}}.log

python -u $SRC_PATH/$EXP $PARAMS $HYPERPARAMETERS| tee -i $OUT_DIR/$LOG_FILE

#word2vec
VECTORS=/Users/theses/THESIS/PROGRAM/DATA/word2vec/semeval_vectors.txt

HYPERPARAMETERS="--name word2vec -ge $VECTORS  -t_oov oov -vl -1 -n_oov 5 -lc True -st False -tk True -rs True"
LOG_FILE=${HYPERPARAMETERS////_}}
LOG_FILE=${LOG_FILE// /_}}.log
python -u $SRC_PATH/$EXP $PARAMS $HYPERPARAMETERS| tee -i $OUT_DIR/$LOG_FILE

HYPERPARAMETERS="--name word2vecfix -ge $VECTORS -rl True -t_oov emb-fix -vl -1 -n_oov 5 -lc True -st False -tk True -rs True"
LOG_FILE=${HYPERPARAMETERS////_}}
LOG_FILE=${LOG_FILE// /_}}.log
python -u $SRC_PATH/$EXP $PARAMS $HYPERPARAMETERS| tee -i $OUT_DIR/$LOG_FILE

exit

#glove
VECTORS="/Users/theses/Developer/glove.6B/glove.6B.300d.txt"


HYPERPARAMETERS="--name glove -ge $VECTORS -t_oov oov -vl -1 -n_oov 5 -lc True -st False -tk True -rs True"
LOG_FILE=${HYPERPARAMETERS////_}}
LOG_FILE=${LOG_FILE// /_}}.log
python -u $SRC_PATH/$EXP $PARAMS $HYPERPARAMETERS| tee -i $OUT_DIR/$LOG_FILE


HYPERPARAMETERS="--name glovefix -ge $VECTORS -rl True  -t_oov emb-fix -vl -1 -n_oov 5 -lc True -st False -tk True -rs True"
LOG_FILE=${HYPERPARAMETERS////_}}
LOG_FILE=${LOG_FILE// /_}}.log
python -u $SRC_PATH/$EXP $PARAMS $HYPERPARAMETERS| tee -i $OUT_DIR/$LOG_FILE

#stem


HYPERPARAMETERS="--name baselinefixstem -t_oov emb-fix -vl -1 -n_oov 5 -lc True -st True -tk True -rs True"
LOG_FILE=${HYPERPARAMETERS////_}}
LOG_FILE=${LOG_FILE// /_}}.log

python -u $SRC_PATH/$EXP $PARAMS $HYPERPARAMETERS| tee -i $OUT_DIR/$LOG_FILE


HYPERPARAMETERS="--name baselinestem -t_oov oov -vl -1 -n_oov 5 -lc True -st True -tk True -rs True"
LOG_FILE=${HYPERPARAMETERS////_}}
LOG_FILE=${LOG_FILE// /_}}.log

python -u $SRC_PATH/$EXP $PARAMS $HYPERPARAMETERS| tee -i $OUT_DIR/$LOG_FILE


VECTORS=/Users/theses/THESIS/PROGRAM/DATA/word2vec/stem_vectors.txt

HYPERPARAMETERS="--name word2vecstem -we $VECTORS  -t_oov oov -vl -1 -n_oov 5 -lc True -st True -tk True -rs True"
LOG_FILE=${HYPERPARAMETERS////_}}
LOG_FILE=${LOG_FILE// /_}}.log
python -u $SRC_PATH/$EXP $PARAMS $HYPERPARAMETERS| tee -i $OUT_DIR/$LOG_FILE

HYPERPARAMETERS="--name word2vecfixstem -we $VECTORS -rl True -t_oov emb-fix -vl -1 -n_oov 5 -lc True -st True -tk True -rs True"
LOG_FILE=${HYPERPARAMETERS////_}}
LOG_FILE=${LOG_FILE// /_}}.log
python -u $SRC_PATH/$EXP $PARAMS $HYPERPARAMETERS| tee -i $OUT_DIR/$LOG_FILE
