#!/usr/bin/env bash

source config.sh

OUT_DIR=$(pwd)/output/sts_rnn
mkdir -p $OUT_DIR

EXP=experiments/sts_experiment.py

#PARAMS="--dataset_root $BASE_PATH/data/semeval-sts/all --train 2015.val.tsv --valid 2015.val.tsv --test 2015.test.tsv --do_not_save"
PARAMS="--dataset_root $BASE_PATH/data/semeval-sts/all --train 2015.train.tsv --valid 2015.val.tsv --test 2015.test.tsv  --do_not_save"


HYPERPARAMETERS="--name rnn --model rnn"
LOG_FILE=${HYPERPARAMETERS// /_}.log
python -u $SRC_PATH/$EXP $PARAMS $HYPERPARAMETERS| tee -i $OUT_DIR/$LOG_FILE

HYPERPARAMETERS="--name rnn --model lstm"
LOG_FILE=${HYPERPARAMETERS// /_}.log
python -u $SRC_PATH/$EXP $PARAMS $HYPERPARAMETERS| tee -i $OUT_DIR/$LOG_FILE


HYPERPARAMETERS="--name rnn --model bilstm"
LOG_FILE=${HYPERPARAMETERS// /_}.log
python -u $SRC_PATH/$EXP $PARAMS $HYPERPARAMETERS| tee -i $OUT_DIR/$LOG_FILE

HYPERPARAMETERS="--name cnn --model cnn"
LOG_FILE=${HYPERPARAMETERS// /_}.log
python -u $SRC_PATH/$EXP $PARAMS $HYPERPARAMETERS| tee -i $OUT_DIR/$LOG_FILE