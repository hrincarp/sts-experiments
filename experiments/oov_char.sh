#!/usr/bin/env bash

source config.sh

OUT_DIR=$(pwd)/output/oov_char
mkdir -p $OUT_DIR

EXP=experiments/oov_experiment.py

#PARAMS="--dataset_root $BASE_PATH/data/semeval-sts/all --train 2015.val.tsv --valid 2015.val.tsv --test 2015.test.tsv --do_not_save"
PARAMS="--dataset_root $BASE_PATH/data/semeval-sts/all --train 2015.train.tsv --valid 2015.val.tsv --test 2015.test.tsv --do_not_save"


HYPERPARAMETERS="--name lstm -m lstm -ed 100 -t_oov oov -vl -1 -n_oov 1 -lc True -st True -tk True -rs True -tt char"
LOG_FILE=${HYPERPARAMETERS// /_}.log
python -u $SRC_PATH/experiments/sts_experiment.py $PARAMS $HYPERPARAMETERS| tee -i $OUT_DIR/$LOG_FILE


HYPERPARAMETERS="--name hash -t_oov hash -hed 2000 -vl -1 -n_oov 0 -lc True -st False -tk False -rs True"
LOG_FILE=${HYPERPARAMETERS// /_}.log
python -u $SRC_PATH/$EXP $PARAMS $HYPERPARAMETERS| tee -i $OUT_DIR/$LOG_FILE


HYPERPARAMETERS="--name cnn -t_oov cnn -vl -1 -n_oov 1 -lc True -st True -tk True -rs True"
LOG_FILE=${HYPERPARAMETERS// /_}.log
python -u $SRC_PATH/$EXP $PARAMS $HYPERPARAMETERS| tee -i $OUT_DIR/$LOG_FILE


HYPERPARAMETERS="--name oov -t_oov oov -vl -100 -n_oov 1 -lc True -st True -tk True -rs True"
LOG_FILE=${HYPERPARAMETERS// /_}.log
python -u $SRC_PATH/$EXP $PARAMS $HYPERPARAMETERS| tee -i $OUT_DIR/$LOG_FILE
