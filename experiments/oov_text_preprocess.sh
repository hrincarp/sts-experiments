#!/usr/bin/env bash

source config.sh

OUT_DIR=$(pwd)/output/oov_text_preprocess
mkdir -p $OUT_DIR

EXP=experiments/oov_experiment.py

#PARAMS="--dataset_root $BASE_PATH/data/semeval-sts/all --train 2015.train.tsv --valid 2015.val.tsv --test 2015.test.tsv --do_not_save"
PARAMS="--dataset_root $BASE_PATH/data/semeval-sts/all --train 2015.train.tsv --valid 2015.val.tsv --test 2015.test.tsv --do_not_save"


HYPERPARAMETERS="--name none -t_oov oov -n_oov 0 -lc False -st False -tk False -rs False"
LOG_FILE=${HYPERPARAMETERS// /_}.log
python -u $SRC_PATH/$EXP $PARAMS $HYPERPARAMETERS| tee -i $OUT_DIR/$LOG_FILE

HYPERPARAMETERS="--name lowercase -t_oov oov -n_oov 0 -lc True -st False -tk False -rs False"
LOG_FILE=${HYPERPARAMETERS// /_}.log
python -u $SRC_PATH/$EXP $PARAMS $HYPERPARAMETERS| tee -i $OUT_DIR/$LOG_FILE

HYPERPARAMETERS="--name tokenizer -t_oov oov -n_oov 0 -lc False -st False -tk True -rs False"
LOG_FILE=${HYPERPARAMETERS// /_}.log
python -u $SRC_PATH/$EXP $PARAMS $HYPERPARAMETERS| tee -i $OUT_DIR/$LOG_FILE

HYPERPARAMETERS="--name stemming -t_oov oov -n_oov 0 -lc False -st True -tk False -rs False"
LOG_FILE=${HYPERPARAMETERS// /_}.log
python -u $SRC_PATH/$EXP $PARAMS $HYPERPARAMETERS| tee -i $OUT_DIR/$LOG_FILE

HYPERPARAMETERS="--name stopwords -t_oov oov -n_oov 0 -lc False -st False -tk False -rs True"
LOG_FILE=${HYPERPARAMETERS// /_}.log
python -u $SRC_PATH/$EXP $PARAMS $HYPERPARAMETERS| tee -i $OUT_DIR/$LOG_FILE

HYPERPARAMETERS="--name all -t_oov oov -n_oov 0 -lc True -st True -tk True -rs True"
LOG_FILE=${HYPERPARAMETERS// /_}.log
python -u $SRC_PATH/$EXP $PARAMS $HYPERPARAMETERS| tee -i $OUT_DIR/$LOG_FILE

