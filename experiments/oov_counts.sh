#!/usr/bin/env bash

source config.sh

OUT_DIR=$(pwd)/output/oov_counts_2
mkdir -p $OUT_DIR

EXP=experiments/oov_experiment.py

PARAMS="--dataset_root $BASE_PATH/data/semeval-sts/all --train 2015.val.tsv --valid 2015.val.tsv --test_files 2015.test.tsv --do_not_save"

HYPERPARAMETERS="--name oov30 -t_oov oov -vl -100 -n_oov 30 -lc True -st True -tk True -rs True"
LOG_FILE=${HYPERPARAMETERS// /_}.log
python -u $SRC_PATH/$EXP $PARAMS $HYPERPARAMETERS| tee -i $OUT_DIR/$LOG_FILE


HYPERPARAMETERS="--name oov0 -t_oov oov -vl -1 -n_oov 0 -lc True -st True -tk True -rs True"
LOG_FILE=${HYPERPARAMETERS// /_}.log
python -u $SRC_PATH/$EXP $PARAMS $HYPERPARAMETERS| tee -i $OUT_DIR/$LOG_FILE


HYPERPARAMETERS="--name oov1 -t_oov oov -vl -1 -n_oov 1 -lc True -st True -tk True -rs True"
LOG_FILE=${HYPERPARAMETERS// /_}.log
python -u $SRC_PATH/$EXP $PARAMS $HYPERPARAMETERS| tee -i $OUT_DIR/$LOG_FILE

HYPERPARAMETERS="--name oov1l -t_oov oov -vl -100 -n_oov 1 -lc True -st True -tk True -rs True"
LOG_FILE=${HYPERPARAMETERS// /_}.log
python -u $SRC_PATH/$EXP $PARAMS $HYPERPARAMETERS| tee -i $OUT_DIR/$LOG_FILE

HYPERPARAMETERS="--name oov1ll -t_oov oov -vl -500 -n_oov 1 -lc True -st True -tk True -rs True"
LOG_FILE=${HYPERPARAMETERS// /_}.log
python -u $SRC_PATH/$EXP $PARAMS $HYPERPARAMETERS| tee -i $OUT_DIR/$LOG_FILE

HYPERPARAMETERS="--name oov2 -t_oov oov -vl -1 -n_oov 2 -lc True -st True -tk True -rs True"
LOG_FILE=${HYPERPARAMETERS// /_}.log
python -u $SRC_PATH/$EXP $PARAMS $HYPERPARAMETERS| tee -i $OUT_DIR/$LOG_FILE

HYPERPARAMETERS="--name oov2l -t_oov oov -vl -100 -n_oov 2 -lc True -st True -tk True -rs True"
LOG_FILE=${HYPERPARAMETERS// /_}.log
python -u $SRC_PATH/$EXP $PARAMS $HYPERPARAMETERS| tee -i $OUT_DIR/$LOG_FILE

HYPERPARAMETERS="--name oov2ll -t_oov oov -vl -500 -n_oov 2 -lc True -st True -tk True -rs True"
LOG_FILE=${HYPERPARAMETERS// /_}.log
python -u $SRC_PATH/$EXP $PARAMS $HYPERPARAMETERS| tee -i $OUT_DIR/$LOG_FILE

HYPERPARAMETERS="--name oov5 -t_oov oov -vl -1 -n_oov 5 -lc True -st True -tk True -rs True"
LOG_FILE=${HYPERPARAMETERS// /_}.log
python -u $SRC_PATH/$EXP $PARAMS $HYPERPARAMETERS| tee -i $OUT_DIR/$LOG_FILE

HYPERPARAMETERS="--name oov5l -t_oov oov -vl -100 -n_oov 5 -lc True -st True -tk True -rs True"
LOG_FILE=${HYPERPARAMETERS// /_}.log
python -u $SRC_PATH/$EXP $PARAMS $HYPERPARAMETERS| tee -i $OUT_DIR/$LOG_FILE

HYPERPARAMETERS="--name oov5ll -t_oov oov -vl -500 -n_oov 5 -lc True -st True -tk True -rs True"
LOG_FILE=${HYPERPARAMETERS// /_}.log
python -u $SRC_PATH/$EXP $PARAMS $HYPERPARAMETERS| tee -i $OUT_DIR/$LOG_FILE


HYPERPARAMETERS="--name oov25 -t_oov oov -vl -1 -n_oov 25 -lc True -st True -tk True -rs True"
LOG_FILE=${HYPERPARAMETERS// /_}.log
python -u $SRC_PATH/$EXP $PARAMS $HYPERPARAMETERS| tee -i $OUT_DIR/$LOG_FILE

HYPERPARAMETERS="--name oov25l -t_oov oov -vl -100 -n_oov 25 -lc True -st True -tk True -rs True"
LOG_FILE=${HYPERPARAMETERS// /_}.log
python -u $SRC_PATH/$EXP $PARAMS $HYPERPARAMETERS| tee -i $OUT_DIR/$LOG_FILE

HYPERPARAMETERS="--name oov25ll -t_oov oov -vl -500 -n_oov 25 -lc True -st True -tk True -rs True"
LOG_FILE=${HYPERPARAMETERS// /_}.log
python -u $SRC_PATH/$EXP $PARAMS $HYPERPARAMETERS| tee -i $OUT_DIR/$LOG_FILE