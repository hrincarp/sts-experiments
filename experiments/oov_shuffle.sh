#!/usr/bin/env bash

source config.sh

OUT_DIR=$(pwd)/output/oov_shuffle
mkdir -p $OUT_DIR

EXP=experiments/oov_experiment.py

PARAMS="--dataset_root $BASE_PATH/data/semeval-sts/all --train 2015.train.tsv --valid 2015.val.tsv --test_files 2015.test.tsv --do_not_save"

HYPERPARAMETERS="--name o2 -t_oov oov -vl -1 -n_oov 30 -lc True -st True -tk True -rs True"
LOG_FILE=${HYPERPARAMETERS// /_}.log
python -u $SRC_PATH/$EXP $PARAMS $HYPERPARAMETERS| tee -i $OUT_DIR/$LOG_FILE
