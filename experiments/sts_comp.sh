#!/usr/bin/env bash

source config.sh

OUT_DIR=$(pwd)/output/sts_comp
mkdir -p $OUT_DIR

EXP=experiments/sts_experiment.py

#PARAMS="--dataset_root $BASE_PATH/data/semeval-sts/all --train 2015.val.tsv --valid 2015.val.tsv --test 2015.test.tsv --do_not_save"
PARAMS="--dataset_root $BASE_PATH/data/semeval-sts/all --train 2015.train.tsv --valid 2015.val.tsv --test 2015.test.tsv  --do_not_save"


HYPERPARAMETERS="--name comp --model sum"
LOG_FILE=${HYPERPARAMETERS// /_}.log
python -u $SRC_PATH/$EXP $PARAMS $HYPERPARAMETERS| tee -i $OUT_DIR/$LOG_FILE

HYPERPARAMETERS="--name comp --model min"
LOG_FILE=${HYPERPARAMETERS// /_}.log
python -u $SRC_PATH/$EXP $PARAMS $HYPERPARAMETERS| tee -i $OUT_DIR/$LOG_FILE


HYPERPARAMETERS="--name comp --model max"
LOG_FILE=${HYPERPARAMETERS// /_}.log
python -u $SRC_PATH/$EXP $PARAMS $HYPERPARAMETERS| tee -i $OUT_DIR/$LOG_FILE

HYPERPARAMETERS="--name comp --model mean"
LOG_FILE=${HYPERPARAMETERS// /_}.log
python -u $SRC_PATH/$EXP $PARAMS $HYPERPARAMETERS| tee -i $OUT_DIR/$LOG_FILE