__author__ = 'frem'

from theano import tensor as T
from blocks.bricks.cost import SquaredError
from tools.mlp import CorrelationCost, Cos_sim
from blocks.bricks.recurrent import LSTM
from blocks.bricks import Tanh
from blocks.bricks.lookup import LookupTable
from blocks.initialization import IsotropicGaussian, Constant
from blocks.bricks import Linear
from sum_embeddings_model import BaseEmbeddingsModel


class SimpleLSTMModel(BaseEmbeddingsModel):
    def __init__(self, tokenizer, name=None):
        super(SimpleLSTMModel, self).__init__(tokenizer=tokenizer)

        s0 = T.ltensor3('s0 index-of-words')
        s0_mask = T.matrix('s0 index-of-words_mask')

        s1 = T.ltensor3('s1 index-of-words')
        s1_mask = T.matrix('s1 index-of-words_mask')

        baseline = T.vector('baseline')

        y = T.vector('similarity')

        dimension = 300

        self.encoder = LSTM(dim=dimension, activation=Tanh(), weights_init=IsotropicGaussian(0.1))
        self.encoder.initialize()

        x_to_h = Linear(name='x_to_h',
                        input_dim=dimension,
                        output_dim=4 * dimension, weights_init=IsotropicGaussian(0.01), use_bias=False)
        x_to_h.initialize()

        self.lookup = LookupTable(self.vocabulary.sequence_width, dimension, weights_init=Constant(self.emb))
        self.lookup.initialize()

        ps0, _ = self.encoder.apply(x_to_h.apply(self.lookup.apply(s0).flatten(3)), mask=s0_mask)
        ps_i0 = T.cast(T.sum(s0_mask, axis=1), dtype='int32') - 1

        ps1, _ = self.encoder.apply(x_to_h.apply(self.lookup.apply(s1).flatten(3)), mask=s1_mask)
        ps_i1 = T.cast(T.sum(s1_mask, axis=1), dtype='int32') - 1

        self.model = Cos_sim().apply(ps1[T.arange(ps1.shape[0]), ps_i1, :],
                                     ps0[T.arange(ps0.shape[0]), ps_i0, :])

        self.model.name = "model"

        self.corr = CorrelationCost().apply(y[:, None], self.model)
        self.corr.name = "corr"

        self.baseline_corr = CorrelationCost().apply(y[:, None], baseline[:, None])
        self.baseline_corr.name = "baseline_corr"

        self.cost = SquaredError().apply(y[:, None], self.model)

        self.cost.name = "cost"

        self.name = name
        if self.name is None:
            self.name = "lstm recurrent"
