__author__ = 'frem'

from theano import tensor as T
from blocks.bricks.cost import SquaredError
from tools.mlp import CorrelationCost, Cos_sim
from blocks.bricks.recurrent import SimpleRecurrent
from blocks.bricks import Tanh, Rectifier
from blocks import initialization
from blocks.bricks.lookup import LookupTable
from blocks.initialization import Constant
from sum_embeddings_model import BaseEmbeddingsModel
from custombricks.conv1d_max import MultiConv1DMaxOverTime


class SimpleRecurrentModel(BaseEmbeddingsModel):
    def __init__(self, tokenizer, name=None):
        super(SimpleRecurrentModel, self).__init__(tokenizer=tokenizer)

        s0 = T.ltensor3('s0 index-of-words')
        s0_mask = T.matrix('s0 index-of-words_mask')

        s1 = T.ltensor3('s1 index-of-words')
        s1_mask = T.matrix('s1 index-of-words_mask')

        baseline = T.vector('baseline')

        y = T.vector('similarity')

        dimension = 300

        self.encoder = SimpleRecurrent(dim=dimension, activation=Rectifier(), weights_init=initialization.IsotropicGaussian(0.1))

        self.encoder.initialize()

        self.lookup = LookupTable(self.vocabulary.sequence_width, dimension, weights_init=Constant(self.emb))
        self.lookup.initialize()

        ps0 = self.encoder.apply(self.lookup.apply(s0).flatten(3), mask=s0_mask)
        ps_i0 = T.cast(T.sum(s0_mask, axis=1), dtype='int32') - 1

        ps1 = self.encoder.apply(self.lookup.apply(s1).flatten(3), mask=s1_mask)
        ps_i1 = T.cast(T.sum(s1_mask, axis=1), dtype='int32') - 1

        self.model = Cos_sim().apply(ps1[T.arange(ps1.shape[0]), ps_i1, :],
                                     ps0[T.arange(ps0.shape[0]), ps_i0, :])



        conv = MultiConv1DMaxOverTime([5], [1], dimension,
                                      weights_init=initialization.IsotropicGaussian(0.1),
                                      biases_init=initialization.IsotropicGaussian(0.1),
                                      activation=Rectifier())
        conv.initialize()
        self.model = Cos_sim().apply(
            conv.apply(T.cast(self.lookup.apply(s0).flatten(3).dimshuffle(1, 0, 2), dtype='float32'), s0_mask.dimshuffle(1, 0)),
            conv.apply(T.cast(self.lookup.apply(s1).flatten(3).dimshuffle(1, 0, 2), dtype='float32'), s1_mask.dimshuffle(1, 0)))


        self.model.name = "model"

        self.corr = CorrelationCost().apply(y[:, None], self.model)
        self.corr.name = "corr"

        self.baseline_corr = CorrelationCost().apply(y[:, None], baseline[:, None])
        self.baseline_corr.name = "baseline_corr"

        self.cost = SquaredError().apply(y[:, None], self.model)

        self.cost.name = "cost"

        self.name = name
        if self.name is None:
            self.name = "cnn 5 300 20"

        self.corr_size = y.shape[0]
        self.corr_size.name = "corr_size"
