__author__ = 'frem'

from theano import tensor as T

from blocks.bricks import MLP, Logistic, Tanh, Rectifier, Identity
from blocks.bricks.cost import SquaredError
from blocks.initialization import IsotropicGaussian, Constant
from tools.mlp import CorrelationCost
from tools.mlp import add_l_norms


class BaseMLPModel(object):
    def __init__(self, input_size, hiddens_layer_size, name=None, activation=Tanh()):
        x = T.matrix('x')
        y = T.vector('y')
        baseline = T.vector('baseline')

        activations = [activation] * len(hiddens_layer_size) + [Logistic()]
        dims = [input_size] + hiddens_layer_size + [1]

        mlp = MLP(activations=activations,
                  dims=dims,
                  weights_init=IsotropicGaussian(1e-3),
                  biases_init=Constant(0), seed=1)
        mlp.initialize()

        self.model = mlp.apply(x) + baseline[:, None]

        self.corr = CorrelationCost().apply(y[:, None], self.model)
        self.corr.name = "corr"

        self.cost = SquaredError().apply(y[:, None], self.model)
        self.cost.name = "cost"

        self.baseline_corr = CorrelationCost().apply(y[:, None], baseline[:, None])
        self.baseline_corr.name = "baseline_corr"

        self.corr_size = y.shape[0]
        self.corr_size.name = "corr_size"

        self.name = name
        if self.name is None:
            self.name = "simple_mlp bow {}".format(hiddens_layer_size)
