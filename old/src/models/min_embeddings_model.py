__author__ = 'frem'
import config
import numpy as np
import theano

from theano import tensor as T
from blocks.bricks.cost import SquaredError
from tools.mlp import CorrelationCost, Cos_sim
from dataset.semeval import SemEvalDataPaths
from blocks.bricks import Linear
from blocks.initialization import IsotropicGaussian, Constant
from custombricks.conv1d_max import MultiConv1DMaxOverTime
from blocks.initialization import Constant
from blocks import initialization
from blocks.bricks import Tanh, Rectifier, Logistic
from models.sum_embeddings_model import BaseEmbeddingsModel
from blocks.bricks.lookup import LookupTable
from blocks.bricks import MLP


class MinEmbeddingsModel(BaseEmbeddingsModel):
    def __init__(self, tokenizer, name=None):
        super(MinEmbeddingsModel, self).__init__(tokenizer=tokenizer)

        s0_len = T.vector('s0 len')
        s1_len = T.vector('s1 len')

        s0 = T.ltensor3('s0 index-of-words')
        s1 = T.ltensor3('s1 index-of-words')

        emb_layer = Linear(name='embeddings',
                        input_dim=self.emb.shape[0],
                        output_dim=self.emb.shape[1], weights_init=Constant(self.emb), use_bias=False)
        emb_layer.initialize()

        baseline = T.vector('baseline')
        y = T.vector('similarity')

        self.lookup = LookupTable(self.emb.shape[0], self.emb.shape[1], weights_init=Constant(self.emb))
        self.lookup.initialize()

        def distance(s0_idxs, s1_idxs):


            def batch(s0_idx, s1_idx):
                def recurrence(x):
                    acc = T.min(Cos_sim().apply(self.lookup.apply(s1_idx).flatten(2), x))
                    return [acc]

                acc, _ = theano.scan(fn=recurrence,
                           sequences=[self.lookup.apply(s0_idx).flatten(2)], outputs_info=[])

                return acc.sum()/acc.shape[0]

            batches, _ = theano.scan(fn=batch,
                           sequences=[s0_idxs, s1_idxs], outputs_info=[])

            return batches

        mlp = MLP(activations=[Rectifier(), Logistic()],
                  dims=[1,5,1],
                  weights_init=IsotropicGaussian(1e-3),
                  biases_init=Constant(0), seed=1)
        mlp.initialize()

        self.model = (distance(s0, s1)+distance(s1, s0))
        self.model = Cos_sim().apply(self.lookup.apply(s0).flatten(3).sum(axis=1), self.lookup.apply(s1).flatten(3).sum(axis=1))

        self.model.name = "min emb"

        self.corr = CorrelationCost().apply(y[:, None], self.model)
        self.corr.name = "corr"

        self.baseline_corr = CorrelationCost().apply(y[:, None], baseline[:, None])
        self.baseline_corr.name = "baseline_corr"

        self.corr_size = y.shape[0]
        self.corr_size.name = "corr_size"

        self.cost = SquaredError().apply(y[:, None], self.model)

        self.cost.name = "cost"

        self.name = name
        if self.name is None:
            self.name = "sum embeddings"
