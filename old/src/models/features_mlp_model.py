__author__ = 'frem'

from theano import tensor as T

from blocks.bricks import MLP, Tanh
from blocks.bricks.cost import SquaredError
from blocks.initialization import IsotropicGaussian, Constant
from blocks.graph import ComputationGraph, apply_dropout
from blocks.filter import VariableFilter
from blocks.roles import *
from tools.mlp import CorrelationCost, Cos_sim

class FeaturesMLPModel(object):
    def __init__(self, input_size, name=None, hiddens=[10], activation=Tanh()):
        s0 = T.matrix('s0 bag-of-words')
        s1 = T.matrix('s1 bag-of-words')

        baseline = T.vector('baseline')

        y = T.vector('similarity')

        activations = [activation] * len(hiddens)
        dims = [input_size] + hiddens

        mlp = MLP(activations=activations,
                  dims=dims,
                  weights_init=IsotropicGaussian(0.01),
                  biases_init=Constant(0), seed=1)
        mlp.initialize()

        self.model = Cos_sim().apply(mlp.apply(s0), mlp.apply(s1))
        self.model_without_dropout = self.model

        # cg = ComputationGraph([self.model])
        # list_of_dropped_out_variables = VariableFilter(roles=[WEIGHT])(cg.variables)
        # cg = apply_dropout(cg, list_of_dropped_out_variables, drop_prob=0.95)

        # self.model = cg.outputs[0]
        self.model.name = "model"

        self.corr = CorrelationCost().apply(y[:, None], self.model_without_dropout)
        self.corr.name = "corr"

        self.baseline_corr = CorrelationCost().apply(y[:, None], baseline[:, None])
        self.baseline_corr.name = "baseline_corr"

        self.corr_size = y.shape[0]
        self.corr_size.name = "corr_size"

        self.cost = SquaredError().apply(y[:, None], self.model)
        self.cost.name = "cost"

        self.name = name
        if self.name is None:
            self.name = "features_from_mlp {}".format(hiddens)