__author__ = 'frem'
import config
import numpy as np
import theano

from theano import tensor as T
from blocks.bricks.cost import SquaredError
from tools.mlp import CorrelationCost, Cos_sim
from dataset.semeval import SemEvalDataPaths
from blocks.bricks import Linear
from custombricks.conv1d_max import MultiConv1DMaxOverTime
from blocks.initialization import Constant
from blocks import initialization
from blocks.bricks import Tanh, Rectifier


class BaseEmbeddingsModel(object):
    def __init__(self, tokenizer):
        self.tokenizer = tokenizer
        self.vocabulary = self.tokenizer.vocabulary
        self.weights = np.random.uniform(0.1, 0.5, self.vocabulary.sequence_width)
        self.emb = self.create_word_embeddings_matrix(SemEvalDataPaths.get_embeddings_file_name())

    def create_word_embeddings_matrix(self, file_name):
        emb = None

        word2index = self.vocabulary.word2index.copy()

        from nltk.stem.porter import PorterStemmer
        stemmer = PorterStemmer()

        skip = True
        f = open(file_name, 'r')
        for l in f:
            if skip:
                skip = False
                continue

            if emb is None:
                emb_size = 50 # len(l.split()[1:])
                emb = (0.09 * np.random.uniform(-1, 1, (self.vocabulary.sequence_width, emb_size)).astype(config.floatX))
                return emb

            word = stemmer.stem(l.split()[0])
            if word in self.vocabulary.word2index:
                word2index.pop(word, None)
                # try:
                emb[self.vocabulary.word2index[word]] = np.array([l.split()[1:]], dtype=config.floatX)
                self.weights[self.vocabulary.word2index[word]] = 1
                # except :
                    # print l

        f.close()

        print word2index.keys()
        print len(word2index)

        return emb


class SumEmbeddingsModel(BaseEmbeddingsModel):
    def __init__(self, tokenizer, name=None):
        super(SumEmbeddingsModel, self).__init__(tokenizer=tokenizer)

        s0 = T.matrix('s0 bag-of-words')
        s1 = T.matrix('s1 bag-of-words')


        s0_len = T.vector('s0 len')
        s1_len = T.vector('s1 len')

        a = theano.shared(1)
        b = theano.shared(1)

        weights = theano.shared(self.weights.astype(config.floatX))

        emb_layer = Linear(name='embeddings',
                        input_dim=self.emb.shape[0],
                        output_dim=self.emb.shape[1], weights_init=Constant(self.emb), use_bias=False)
        emb_layer.initialize()

        baseline = T.vector('baseline')

        y = T.vector('similarity')

        self.model = Cos_sim().apply(emb_layer.apply(s0), emb_layer.apply(s1))
        #      emb_layer.apply(weights * s1) / (weights * s1).sum(axis=1)[:, None], # ((weights * s1).sum(axis=0)),
        #      emb_layer.apply(weights * s0) / (weights * s0).sum(axis=1)[:, None] #((weights * s0).sum(axis=0)/))
        # )

        self.model = a*self.model # + b*baseline[:, None]

        self.model.name = "model"

        self.corr = CorrelationCost().apply(y[:, None], self.model)
        self.corr.name = "corr"

        self.baseline_corr = CorrelationCost().apply(y[:, None], baseline[:, None])
        self.baseline_corr.name = "baseline_corr"

        self.corr_size = y.shape[0]
        self.corr_size.name = "corr_size"

        self.cost = SquaredError().apply(y[:, None], self.model)

        self.cost.name = "cost"

        self.name = name
        if self.name is None:
            self.name = "sum embeddings"
