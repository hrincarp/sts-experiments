import experiment
experiment.set_path()

from argparse import ArgumentParser
from models.sum_embeddings_model import SumEmbeddingsModel
from dataset.semeval import BagOfWordsSeparateSentenceDataStreamFactory, SemEvalData
from cross_validation import cross_validation
from dataset.dataset_transformers import Vocabulary
from models.min_embeddings_model import MinEmbeddingsModel


def sum_embeddings_bow(batch_size, num_epochs):
    data = SemEvalData(which_set="train")

    msr_data = SemEvalData(which_set="msr")

    data.annotations += msr_data.annotations
    data.sentences += msr_data.sentences

    factory_stream = BagOfWordsSeparateSentenceDataStreamFactory()
    data_stream = factory_stream.create_data_stream(data)

    all_test_data = SemEvalData.get_all_test()
    test_streams = [(name, factory_stream.create_data_stream(test_data)) for name, test_data in all_test_data]

    model = SumEmbeddingsModel(tokenizer=factory_stream.tokenizer,
                            )

    cross_validation(model=model,
                     data_stream=data_stream,
                     test_streams=test_streams,
                     batch_size=batch_size,
                     num_folds=2,
                     num_epochs=num_epochs,
                     sources=['baseline', 's0 bag-of-words', 's1 bag-of-words', 's0 len', 's1 len', 'similarity'])


if __name__ == "__main__":
    parser = ArgumentParser("Model uses embeddings.")
    parser.add_argument("--num-epochs", type=int, default=100,
                        help="Number of training epochs to do.")
    parser.add_argument("--batch-size", type=int, default=100,
                        help="Batch size.")
    args = parser.parse_args()

    sum_embeddings_bow(**vars(args))
