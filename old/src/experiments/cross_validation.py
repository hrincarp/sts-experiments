from tools.mlp import create_main_loop
from fuel.transformers import FilterSources, Filter, Batch, Padding
from fuel.schemes import ConstantScheme, ConcatenatedScheme, SequentialScheme
from fuel.transformers import Merge
from blocks.graph import ComputationGraph
import numpy as np
import theano


def cross_validation(model, data_stream, test_streams, batch_size, num_folds, sources, num_epochs, num_times=1):
    test_streams = [(name, Padding(Batch(test_stream, iteration_scheme=ConstantScheme(batch_size=100000)),
                                   mask_sources=["s0 index-of-words", "s1 index-of-words"]))
                   for name, test_stream in test_streams] # basically all data

    for i in range(num_times):
        def train_predicate(value):
            return True # value[0] % num_folds != i

        def valid_predicate(value):
            return value[0] % num_folds == i

        train_stream = Filter(data_stream, train_predicate)
        train_stream = Padding(Batch(train_stream, iteration_scheme=ConstantScheme(batch_size=batch_size)),
                               mask_sources=["s0 index-of-words", "s1 index-of-words"])

        train_stream = FilterSources(train_stream, sources=sources)

        valid_stream = Filter(data_stream, valid_predicate)
        valid_stream = Padding(Batch(valid_stream, iteration_scheme=ConstantScheme(batch_size=batch_size)),
                               mask_sources=["s0 index-of-words", "s1 index-of-words"])

        main_loop = create_main_loop(model=model,
                                     train_stream=train_stream,
                                     validation_stream=valid_stream,
                                     test_streams=test_streams,
                                     num_epochs=num_epochs,
                                     experiment_name="{} ({}-{})".format(model.name, i+1, num_folds))

        main_loop.run()