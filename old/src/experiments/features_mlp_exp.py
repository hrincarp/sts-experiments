import experiment
experiment.set_path()

from argparse import ArgumentParser
from models.features_mlp_model import FeaturesMLPModel
from dataset.semeval import BagOfWordsSeparateSentenceDataStreamFactory, SemEvalData
from cross_validation import cross_validation


def mlp_bow(hiddens, batch_size, num_epochs):
    data = SemEvalData(which_set="train")

    msr_data = SemEvalData(which_set="msr")

    data.annotations += msr_data.annotations
    data.sentences += msr_data.sentences

    factory_stream = BagOfWordsSeparateSentenceDataStreamFactory()
    data_stream = factory_stream.create_data_stream(data)

    all_test_data = SemEvalData.get_all_test()
    test_streams = [(name, factory_stream.create_data_stream(test_data)) for name, test_data in all_test_data]

    model = FeaturesMLPModel(input_size=factory_stream.get_input_size(),
                                hiddens=hiddens,
                            )

    cross_validation(model=model,
                     data_stream=data_stream,
                     test_streams=test_streams,
                     batch_size=batch_size,
                     num_folds=10,
                     num_epochs=num_epochs,
                     sources=['s0 bag-of-words', 's1 bag-of-words', 'similarity'])


if __name__ == "__main__":
    parser = ArgumentParser("Model uses mlp to select featured vector.")
    parser.add_argument("--num-epochs", type=int, default=100,
                        help="Number of training epochs to do.")
    parser.add_argument("--hiddens", type=int, nargs='+',
                        default=[500], help="List of numbers of hidden units for the MLP.")
    parser.add_argument("--batch-size", type=int, default=100,
                        help="Batch size.")
    args = parser.parse_args()

    mlp_bow(**vars(args))
