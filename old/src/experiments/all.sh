#!/bin/bash

THEANO_FLAGS=floatX=float32,device=gpu python -c "
import sys; sys.path += ['./..'];

execfile('features_mlp_exp.py')
execfile('simple_mlp_exp.py')
execfile('sum_embeddings_exp.py')
execfile('simple_lstm_exp.py')
execfile('simple_recurrent_exp.py')
"