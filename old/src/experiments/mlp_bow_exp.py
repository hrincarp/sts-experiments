import experiment
experiment.set_path()

from argparse import ArgumentParser
from models.base_mlp_model import BaseMLPModel
from dataset.semeval import BagOfWordsDataStreamFactory, SemEvalData
from cross_validation import cross_validation


def mlp_bow(hiddens, batch_size, num_epochs):
    data = SemEvalData(which_set="train")

    factory_stream = BagOfWordsDataStreamFactory()
    data_stream = factory_stream.create_data_stream(data)

    all_test_data = SemEvalData.get_all_test()

    test_streams = [(name, factory_stream.create_data_stream(test_data)) for name, test_data in all_test_data]

    model = BaseMLPModel(input_size=factory_stream.get_input_size(),
                        hiddens_layer_size=hiddens,
                        name="mlp_bow"
                        )

    cross_validation(model=model,
                     data_stream=data_stream,
                     test_streams=test_streams,
                     batch_size=batch_size,
                     num_folds=2,
                     num_epochs=num_epochs,
                     sources=['x', 'y', 'baseline'])


if __name__ == "__main__":
    parser = ArgumentParser("Model uses mlp to predict similarity.")
    parser.add_argument("--num-epochs", type=int, default=100,
                        help="Number of training epochs to do.")
    parser.add_argument("--hiddens", type=int, nargs='+',
                        default=[1000], help="List of numbers of hidden units for the MLP.")
    parser.add_argument("--batch-size", type=int, default=100,
                        help="Batch size.")
    args = parser.parse_args()

    mlp_bow(**vars(args))
