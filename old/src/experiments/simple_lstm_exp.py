import experiment
experiment.set_path()

from argparse import ArgumentParser
from models.simple_lstm_model import SimpleLSTMModel
from dataset.semeval import SequenceIndexOfWordsDataStreamFactory, SemEvalData
from cross_validation import cross_validation


def simple_recurrent(batch_size, num_epochs):
    data = SemEvalData(which_set="train")

    factory_stream = SequenceIndexOfWordsDataStreamFactory()
    data_stream = factory_stream.create_data_stream(data)

    all_test_data = SemEvalData.get_all_test()
    test_streams = [(name, factory_stream.create_data_stream(test_data)) for name, test_data in all_test_data]

    model = SimpleLSTMModel(tokenizer=factory_stream.tokenizer,
                            )

    cross_validation(model=model,
                     data_stream=data_stream,
                     test_streams=test_streams,
                     batch_size=batch_size,
                     num_folds=10,
                     num_epochs=num_epochs,
                     sources=['s0 index-of-words', 's1 index-of-words', 'similarity', 's0 index-of-words_mask', 's1 index-of-words_mask'])


if __name__ == "__main__":
    parser = ArgumentParser("Model uses embeddings.")
    parser.add_argument("--num-epochs", type=int, default=50,
                        help="Number of training epochs to do.")
    parser.add_argument("--batch-size", type=int, default=5,
                        help="Batch size.")
    args = parser.parse_args()

    simple_recurrent(**vars(args))
