from blocks.bricks import Initializable, application, lazy
from blocks.bricks.conv import Convolutional
from theano.tensor.nnet import conv2d
import theano.tensor as tt



class Convolutional1D(Convolutional):
    """Performs a 1D convolution.

    Parameters
    ----------
    filter_size : tuple
        The height and width of the filter (also called *kernels*).
    num_filters : int
        Number of filters per channel.
    step : tuple, optional
        The step (or stride) with which to slide the filters over the
        sequence. Defaults to (1, 1).


    """
#    @lazy(allocation=['filter_size', 'num_filters', 'num_channels'])
#    def __init__(self, filter_size, num_filters, batch_size=None,
#                 image_size=None, step=(1, 1), border_mode='valid',
#                 tied_biases=False, pad_sequences=True, **kwargs):
    def __init__(self, pad_sequences=True, **kwargs):

        kwargs['num_channels'] = 1
        kwargs['tied_biases'] = True

        super(Convolutional1D, self).__init__(**kwargs)

        self.pad_sequences = pad_sequences
        self.filter_length = kwargs['filter_size'][0]



    @application(inputs=['input_'], outputs=['output'])
    def apply(self, input_):
        """Perform the convolution.

        Parameters
        ----------
        input_ : :class:`~tensor.TensorVariable`
            A 4D tensor with the axes representing batch size, number of
            channels, image height, and image width.

        Returns
        -------
        output : :class:`~tensor.TensorVariable`
            A 4D tensor of filtered images (feature maps) with dimensions
            representing batch size, number of filters, feature map height,
            and feature map width.

            The height and width of the feature map depend on the border
            mode. For 'valid' it is ``image_size - filter_size + 1`` while
            for 'full' it is ``image_size + filter_size - 1``.

        """

        if self.pad_sequences:
            # input is padded by zeros from both sides
            padding = tt.alloc(0, self.filter_length - 1, input_.shape[1], input_.shape[2])
            input_ = tt.concatenate((padding, input_, padding), axis=0)


        # transform input from bricks (time x batch x features) to Theano's (batch, stack, row aka time, col aka features)
        conv_input = input_.dimshuffle(1, 'x', 0, 2)
        #conv_input = tt.printing.Print("conv_input")(conv_input)

        conved = super(Convolutional1D,self).apply(conv_input)
        # output has this form (batch size, nb filters, output row, output col)

        # output has only one column
        conved = conved[:,:,:,0]

        # convert back to rnn like format
        return conved.dimshuffle(2,0,1)


    def get_dim(self, name):
        if name == 'input_':
            return (self.num_channels,) + self.image_size
        if name == 'output':
            return ((self.num_filters,) +
                    ConvOp.getOutputShape(self.image_size, self.filter_size,
                                          self.step, self.border_mode))
        return super(Convolutional1D, self).get_dim(name)
