from numpy import inf

from blocks.bricks import Initializable, application, lazy, Feedforward
from blocks.bricks.conv import Convolutional
from theano.tensor.nnet import conv2d
import theano.tensor as tt


class MaxOverTime(Feedforward):
    """Max pooling layer.

    Parameters
    ----------


    """
    # TODO
    #@lazy(allocation=['pooling_size'])
    def __init__(self, **kwargs):
        super(MaxOverTime, self).__init__(**kwargs)

    @application(inputs=['input', 'mask'], outputs=['output'])
    def apply(self, input, mask):
        """Apply the max pooling over time transformation.

        Parameters
        ----------
        input_ : :class:`~tensor.TensorVariable`
            A tensor with dimensions (time X batch X features).

        Returns
        -------
        output : :class:`~tensor.TensorVariable`
            A tensor with dimension (batch X features) where features store the maximum value over time.

        """

        # TODO maybe instead of computing min we can use NaN or some other value
        # get minimum value from all elements
        min_val = tt.min(input)

        # create matrix with min values everywhere
        masking_value_matrix = tt.fill(input, min_val)

        mask_3d = mask.dimshuffle(0, 1, 'x')

        # use mask to put -inf everywhere where the mask has 0
        masked = input * mask_3d + (1 - mask_3d) * masking_value_matrix



        # get maximum over time
        output = tt.max(masked, axis=0)
        return output

    def get_dim(self, name):
        if name == 'mask':
            return 0
        return 0
        """
        if name == 'input':
            return self.input_dim
        if name == 'output':
            return tuple(DownsampleFactorMax.out_shape(self.input_dim,
                                                       self.pooling_size,
                                                       st=self.step))
        """
