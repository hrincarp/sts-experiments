__author__ = 'frem'

import theano
floatX = "float32"
theano.config.floatX = floatX
theano.config.optimizer = 'None'
theano.exception_verbosity = 'high'

import os
SRC_DIRECTORY = os.path.dirname(os.path.realpath(__file__)) + "/"
DATA_DIRECTORY = SRC_DIRECTORY + '../DATA/'

MODELS_DATA = SRC_DIRECTORY + 'models_data/'

import logging
logging.disable(logging.CRITICAL)