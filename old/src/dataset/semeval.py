__author__ = 'frem'

import config

import sys
from fuel.datasets import IterableDataset, IndexableDataset
from dataset_transformers import *
from fuel.schemes import ConstantScheme
from fuel.transformers import Batch, Cast
from fuel.transformers import FilterSources, Filter, Unpack, Batch
import tools


class SemEvalDataPaths(object):
    """
    This class contains paths of SemEval data sets and functions to read it
    """
    @staticmethod
    def get_mrs_file_name():
        return config.DATA_DIRECTORY + '/MSRParaphraseCorpus/msr_paraphrase_train.txt'

    @staticmethod
    def get_embeddings_file_name():
        return config.DATA_DIRECTORY + '/word2vec/semeval_stem_vectors.txt'

    @staticmethod
    def get_all_train_data_input_file_name():
        return config.DATA_DIRECTORY + '/SemEval/all.train.input.txt.seded'

    @staticmethod
    def get_all_train_data_gs_file_name():
        return config.DATA_DIRECTORY + '/SemEval/all.train.gs.txt'

    @staticmethod
    def get_all_test_data_input_file_name():
        return config.DATA_DIRECTORY + '/SemEval/clean.test.input.txt.seded'

    @staticmethod
    def get_all_test_data_gs_file_name():
        return config.DATA_DIRECTORY + '/SemEval/clean.test.gs.txt'

    @staticmethod
    def get_all_test_data_gs_files_names():
        return [
            config.DATA_DIRECTORY + '/SemEval/TEST-STS2015/STS.gs.images.txt',
            config.DATA_DIRECTORY + '/SemEval/TEST-STS2015/STS.gs.answers-forums.txt',
            config.DATA_DIRECTORY + '/SemEval/TEST-STS2015/STS.gs.answers-students.txt',
            config.DATA_DIRECTORY + '/SemEval/TEST-STS2015/STS.gs.belief.txt',
            config.DATA_DIRECTORY + '/SemEval/TEST-STS2015/STS.gs.headlines.txt',
        ]

    @staticmethod
    def get_all_test_data_input_files_names():
        return [
            config.DATA_DIRECTORY + '/SemEval/TEST-STS2015/STS.input.images.txt',
            config.DATA_DIRECTORY + '/SemEval/TEST-STS2015/STS.input.answers-forums.txt',
            config.DATA_DIRECTORY + '/SemEval/TEST-STS2015/STS.input.answers-students.txt',
            config.DATA_DIRECTORY + '/SemEval/TEST-STS2015/STS.input.belief.txt',
            config.DATA_DIRECTORY + '/SemEval/TEST-STS2015/STS.input.headlines.txt',
        ]

    #max_senteces = 50
    max_senteces = sys.maxint

    @staticmethod
    def read_sentences(filename, start=0, end=max_senteces):
        f = open(filename)
        sentences = []

        i = 0
        for l in f:
            l = l.decode('utf-8')
            if start <= i < end:
                sentences += [l.split("\t")]
            i += 1
        f.close()

        return sentences

    @staticmethod
    def read_msr(filename, start=0, end=max_senteces):
        annotations = []
        sentences = []
        with open(filename, 'r') as f:
            i = 0

            for l in f:
                l = l.decode('utf-8')
                if i == 0:
                    i += 1
                    continue
                if start <= i < end:
                    annotations += [float(l.split("\t")[0])]
                    sentences += [[l.split('\t')[3], l.split('\t')[4]]]

                i += 1
            f.close()

        return sentences, annotations

    @staticmethod
    def read_sim_scores(filename, start=0, end=max_senteces):
        f = open(filename)
        sim_scores = []

        i = 0
        for l in f:
            if start <= i < end:
                try:
                    sim_scores += [float(l)/5]
                except ValueError:
                    sim_scores += [-1]
            i += 1
        f.close()

        return sim_scores


class SemEvalData(object):
    def __init__(self, which_set=None, sentences=None, annotations=None, data_input_file=None, data_gs_file=None):
        """
        class reads sentences and annotations
        :param data_input_file:
        :param data_gs_file:
        :param which_data: "train", "validation", "test
        :return:
        """

        if which_set == "train":
            data_input_file = SemEvalDataPaths.get_all_train_data_input_file_name()
            data_gs_file = SemEvalDataPaths.get_all_train_data_gs_file_name()

        if which_set == 'msr':
            self.sentences, self.annotations = SemEvalDataPaths.read_msr(SemEvalDataPaths.get_mrs_file_name())
            return

        if which_set == "test":
            data_input_file = SemEvalDataPaths.get_all_test_data_input_file_name()
            data_gs_file = SemEvalDataPaths.get_all_test_data_gs_file_name()

        if data_input_file is not None:
            self.sentences = SemEvalDataPaths.read_sentences(data_input_file)
        else:
            self.sentences = sentences

        if data_gs_file is not None:
            self.annotations = SemEvalDataPaths.read_sim_scores(data_gs_file)
        else:
            self.annotations = annotations


    @staticmethod
    def get_all_test():
        test_data = []
        for f_input, f_gs in zip(SemEvalDataPaths.get_all_test_data_input_files_names(), SemEvalDataPaths.get_all_test_data_gs_files_names()):
            test_data += [(f_input.split(".")[-2],  SemEvalData(data_input_file=f_input, data_gs_file=f_gs))]

        return test_data


class SemEvalDataSet(object):
    def __init__(self, sources):
        self.sources = sources

    def split(self, num_folds=10):
        folds = []
        for i in range(0, num_folds):
            folds[i] = {}
            for k, source in self.sources.iteritems():
                folds[i][k] = [x for i, x in enumerate(source) if i % num_folds == i]

        return folds

    def create_data_set(self, sources=None):
        if sources is None:
            sources = self.sources

        return IterableDataset(sources)


class BaseModelDataStreamFactory(object):

    def get_input_size(self):
        raise NotImplementedError

    def create_data_set(self, data, baseline):
        raise NotImplementedError

    def create_data_stream(self, data):
        dataset = self.create_data_set(data)
        data_stream = dataset.get_example_stream()

        # remove with negative index
        def predicate(value):
            return value[1] >= 0

        data_stream = Filter(data_stream, predicate)

        return data_stream


class BagOfWordsDataStreamFactory(BaseModelDataStreamFactory):
    def __init__(self, vocabulary_size=20000):
        self.tokenizer = self.create_tokenizer(vocabulary_size)

    def get_input_size(self):
        return self.tokenizer.sequence_width * 2

    def create_tokenizer(self, vocabulary_size):
        with open(SemEvalDataPaths.get_all_test_data_input_file_name()) as f:
            text = f.read().decode('utf-8')

        return Tokenizer(vocabulary_size, text, stem=True, remove_stopwords=True, lower=True, nltk_tokenize=True)

    def create_data_set(self, data):

        sentences = data.sentences
        annotations = data.annotations

        sentences = TokenizerTransformer(self.tokenizer).map(sentences)
        cos_sim = [tools.cos_sim(s0, s1) for s0, s1 in sentences]

        sentences = UnigramTransformer(self.tokenizer).map(sentences)
        sentences = MaxPoolingIndexesTransformer(self.tokenizer).map(sentences)
        sentences = ConcatenateSentencesTransformer().map(sentences)

        annotations = np.array(annotations, dtype=config.floatX)
        sentences = np.array(sentences, dtype=config.floatX)
        cos_sim = np.array(cos_sim, dtype=config.floatX)

        from collections import OrderedDict
        sources = OrderedDict([
            ("_index", range(0, len(annotations))),
            ("y", annotations),
            ("x", sentences),
            ("baseline", cos_sim)
        ])

        return IndexableDataset(sources)

class AdvanceBagOfWordsDataStreamFactory(BagOfWordsDataStreamFactory):
    def __init__(self, vocabulary_size=20000):
        self.tokenizer = self.create_tokenizer(vocabulary_size)

    def get_input_size(self):
        return 6 + self.tokenizer.sequence_width * 6

    def create_data_set(self, data):

        sentences = data.sentences
        sentences = TokenizerTransformer(self.tokenizer).map(sentences)

        annotations = data.annotations
        annotations = np.array(annotations, dtype=config.floatX)

        cos_sim = [tools.cos_sim(s0, s1) for s0, s1 in sentences]
        cos_sim = np.array(cos_sim, dtype=config.floatX)

        features = SimpleFeatureTransformer().map(sentences)
        features = ConcatenateSentencesTransformer().map(zip([np.array([cs]) for cs in cos_sim], features))

        features = np.array(features, dtype=config.floatX)

        sentences = UnigramTransformer(self.tokenizer).map(sentences)
        sentences = MaxPoolingIndexesTransformer(self.tokenizer).map(sentences)
        sentences = DifferenceTransformer().map(sentences)

        sentences = ConcatenateSentencesTransformer().map(sentences)
        sentences = ConcatenateSentencesTransformer().map(zip(sentences, features))
        sentences = np.array(sentences, dtype=config.floatX)

        from collections import OrderedDict
        sources = OrderedDict([
            ("_index", range(0, len(annotations))),
            ("y", annotations),
            ("x", sentences),
            ("baseline", cos_sim)
        ])

        return IndexableDataset(sources)


class SequenceIndexOfWordsDataStreamFactory(BagOfWordsDataStreamFactory):
    def create_data_set(self, data):

        sentences = data.sentences
        annotations = data.annotations

        annotation = np.array(annotations, dtype=config.floatX)

        cos_sim = [tools.cos_sim(s0.split(), s1.split()) for s0, s1 in sentences]

        sentences = TokenizerTransformer(self.tokenizer).map(sentences)
        cos_sim = [tools.cos_sim(s0, s1) for s0, s1 in sentences]
        cos_sim = np.array(cos_sim, dtype=config.floatX)

        sentences = UnigramTransformer(self.tokenizer).map(sentences)
        sentences = IndexesTransformer().map(sentences)

        sentences = [[s0 for s0, _ in sentences], [s1 for _, s1 in sentences]]

        from collections import OrderedDict
        sources = OrderedDict([
            ("_index", range(0, len(annotation))),
            ("similarity", annotation),
            ("s0 index-of-words", sentences[0]),
            ("s1 index-of-words", sentences[1]),
            ("baseline", cos_sim)
        ])

        return IndexableDataset(sources)


class BagOfWordsSeparateSentenceDataStreamFactory(BagOfWordsDataStreamFactory):
    def get_input_size(self):
        return self.tokenizer.sequence_width


    def create_tokenizer(self, vocabulary_size):
        with open(SemEvalDataPaths.get_all_test_data_input_file_name()) as f:
            text = f.read().decode('utf-8')

        # with open(SemEvalDataPaths.get_all_train_data_input_file_name()) as f:
        #     text += f.read().decode('utf-8')

        return Tokenizer(vocabulary_size, text, stem=True, remove_stopwords=True, lower=True, nltk_tokenize=True)

    def create_data_set(self, data):
        sentences = data.sentences
        annotations = data.annotations

        sentences_len0 = np.array([len(s0) for s0, _ in sentences], dtype=config.floatX)
        sentences_len1 = np.array([len(s1) for _, s1 in sentences], dtype=config.floatX)

        annotation = np.array(annotations, dtype=config.floatX)
        cos_sim = [tools.cos_sim(s0.split(), s1.split()) for s0, s1 in sentences]

        sentences = TokenizerTransformer(self.tokenizer).map(sentences)
        cos_sim = [tools.cos_sim(s0, s1) for s0, s1 in sentences]
        cos_sim = np.array(cos_sim, dtype=config.floatX)

        sentences = UnigramTransformer(self.tokenizer).map(sentences)
        sentences = MaxPoolingIndexesTransformer(self.tokenizer).map(sentences)

        sentences = [[s0 for s0, _ in sentences], [s1 for _, s1 in sentences]]

        from collections import OrderedDict
        sources = OrderedDict([
            ("_index", range(0, len(annotation))),
            ("similarity", annotation),
            ("s0 bag-of-words", sentences[0]),
            ("s1 bag-of-words", sentences[1]),

            ("s0 len", sentences_len0),
            ("s1 len", sentences_len1),
            ("baseline", cos_sim)
        ])

        return IndexableDataset(sources)