__author__ = 'frem'

from fuel.transformers import SourcewiseTransformer
from nltk.tokenize import word_tokenize
import nltk
from nltk.stem.porter import PorterStemmer
import numpy as np
import config


class DatasetTransformer(SourcewiseTransformer):
    """
    Base class of Transformer for dataset (or datastream)
    """
    def __init__(self, data_stream=None, which_sources=None):
        if which_sources is not None:
            super(DatasetTransformer, self).__init__(data_stream, which_sources=which_sources)

    def map(self, data):
        return [self.transform(source) for source in data]

    def transform(self, source):
        pass


class SimpleFeatureTransformer(DatasetTransformer):
    def __init__(self, data_stream=None, which_sources=None):
        super(SimpleFeatureTransformer, self).__init__(data_stream=data_stream, which_sources=which_sources)

    def transform(self, source):
        return np.array([len(set(source[0])),
                         len(set(source[1])),
                         len(set(source[1]).intersection(set(source[0]))),
                         len(set(source[1]).difference(set(source[0]))),
                         len(set(source[0]).difference(set(source[1])))
                         ])


class ConcatenateSentencesTransformer(DatasetTransformer):
    def __init__(self, data_stream=None, which_sources=None):
        super(ConcatenateSentencesTransformer, self).__init__(data_stream=data_stream, which_sources=which_sources)

    def transform(self, source):
        return np.concatenate([s for s in source])


class DifferenceTransformer(DatasetTransformer):
    def __init__(self, data_stream=None, which_sources=None):
        super(DifferenceTransformer, self).__init__(data_stream=data_stream, which_sources=which_sources)

    def transform(self, source):
        return [source[0], source[1],
                np.array(source[0] != source[1], dtype=config.floatX),
                np.array((source[0] == source[1]) == (source[1] != np.zeros_like(source[0])), dtype=config.floatX),
                np.array(source[0] > source[1], dtype=config.floatX),
                np.array(source[0] < source[1], dtype=config.floatX)
                ]


class MaxPoolingIndexesTransformer(DatasetTransformer):
    def __init__(self, tokenizer, data_stream=None, which_sources=None):
        self.tokenizer = tokenizer
        super(MaxPoolingIndexesTransformer, self).__init__(data_stream=data_stream, which_sources=which_sources)

    def transform(self, source):
        return [self.indexes_to_vector(s) for s in source]

    def indexes_to_vector(self, indexes):
        result = np.zeros(self.tokenizer.sequence_width, dtype=config.floatX)

        word_row = 0
        for word_index in indexes:
            result[word_index] = 1
            word_row += 1

        return result


class IndexesTransformer(DatasetTransformer):

    def transform(self, source):
        return [self.sentence_to_indexes(s) for s in source]

    def sentence_to_indexes(self, sequence):
        indexes = np.array([[s] for s in sequence], dtype=int)
        return indexes

class UnigramTransformer(DatasetTransformer):
    def __init__(self, tokenizer, data_stream=None, which_sources=None):
        self.tokenizer = tokenizer
        super(UnigramTransformer, self).__init__(data_stream, which_sources=which_sources)

    def transform(self, source):
        return [self.sentence_to_indexes(s) for s in source]

    def sentence_to_indexes(self, sentence):
        indexes = [self.tokenizer.vocabulary.get_index_for_word(s) for s in sentence]
        return indexes


class TokenizerTransformer(DatasetTransformer):
    def __init__(self, tokenizer, data_stream=None, which_sources=None):
        self.tokenizer = tokenizer
        super(TokenizerTransformer, self).__init__(data_stream, which_sources=which_sources)

    def transform(self, source):
        return [self.tokenizer.advance_tokenize(s) for s in source]


class Tokenizer():
    def __init__(self, vocabulary_size, text="", stem=True, remove_stopwords=True, lower=True, nltk_tokenize=True):
        self.stem = stem
        self.remove_stopwords = remove_stopwords
        self.lower = lower
        self.nltk_tokenize = nltk_tokenize
        self.stemmer = PorterStemmer()

        self.max_vocabulary_size = vocabulary_size
        self.vocabulary = self.create_vocabulary(text)
        self.sequence_width = self.vocabulary.sequence_width

    def advance_tokenize(self, text, return_set=False):
        if self.lower:
            text = text.lower()

        if self.nltk_tokenize:
            tokens = word_tokenize(text)
        else:
            tokens = text.split()

        if return_set:
            tokens = set(tokens)

        if self.remove_stopwords:
            stopwords = set(nltk.corpus.stopwords.words('english'))
            if return_set:
                tokens = tokens - stopwords
            else:
                tokens = list(set(tokens) - stopwords)

        if self.stem:
            tokens = [self.stemmer.stem(word) for word in tokens]

        return tokens


    def create_vocabulary(self, text):
        d = {}
        tokens = self.advance_tokenize(text, return_set=True)
        tokens = set(tokens) - \
                 set([u'accus', u'authoris', u'neocon/teabagg', u'propens', u'bateri', u'counterpropos', u'phylosoph', u'rhymezon', u'generalis', u'termnal', u'refus', u'sewol', u'parralel', u'greenhous', u'seeded/rank', u'on/in', u'headand', u'fuckker', u'vietnames', u'negavt', u'advertis', u'interefer', u'dfferent', u'negative/unform', u'electri', u'sterilis', u'pataliputra', u'btteri', u'deixi', u'connct', u'deshedd', u'polaris', u'trekpod', u'airalgeri', u'compens', u'rslw', u'lighthous', u'claus', u'backgroud', u'intrus', u'africal', u'extrovers', u'quartermass', u'abus', u'state/federal/provincial/territori', u'perus', u'thebulb', u'yongnuo', u'acceler', u'comprehens', u'facor', u'blaa', u'unsubstati', u'bikeboy', u'afaict', u'dimension', u'zombietim', u'exercis', u'pelastr', u'they/their', u'conclus', u'represent', u'consider', u'shitstain', u'lamarkian', u'fals', u'habenera', u'pocketwizard', u'shereka', u'atteri', u'explos', u'namvet', u'becuas', u'reacion', u'thanosk', u'serper', u'demwhit', u'pearsonartphoto', u'differentclos', u'male/femal', u'frisbe', u'socialists/progress', u'bbulb', u'scall', u'bigish', u'recognis', u'inclus', u'curs', u'lens', u'seawe', u'elsewher', u'greas', u'lebanes', u'obvious', u'paraphras', u'alliter', u'zentralblatt', u'willing', u'sit/stand', u'overal', u'contiain', u'timelaps', u'hypernym', u'upris', u'connedt', u'bowling/bat', u'seteroper', u'occasion', u'nois', u'goivern', u'disagre', u'precsis', u'tourquois', u'japanes', u'prais', u'choos', u'scottsman', u'realis', u'april/may/jun', u'waterfal', u'elinchrom', u'ttermin', u'extarn', u'legalis', u'sudanes', u'invas', u'reation', u'forens', u'reced', u'insignific', u'spous', u'exclus', u'diagnos', u'equival', u'unpreced', u'workflowi', u'separart', u'becaqus', u'his/herself', u'palatalis', u'senegales', u'volleybal', u'excus', u'batterti', u'ywriter', u'logic/math', u'gees', u'linhof', u'databas', u'nonsens', u'pregnancircumst', u'surrond', u'congression', u'jooz', u'expos', u'toprop', u'predec', u'standing/sit', u'serious', u'pposit', u'animate/inanim', u'subvers', u'terminak', u'purpos', u'swithch', u'preced', u'practis', u'dyansti', u'supersed', u'curios', u'provision', u'ozbu', u'rational', u'rins', u'equpit', u'emphasis', u'cloos', u'anythi', u'commentri', u'cultorom', u'culturom', u'ossiah', u'intrins', u'aris', u'contai', u'gnois', u'emphas', u'geocultur', u'srgb', u'mathscinet', u'/k/', u'capewrathtrail', u'technique/srt', u'made/diy', u'premis', u'bathingsuit', u'keltari', u'decreas', u'discours', u'religioiu', u'arcmin', u'nigrum', u'corps', u'bibbl', u'knowing/understand', u'negtiv', u'purchas', u'aavso', u'bewtween', u'caus', u'hard/imposs', u'ceas', u'refere', u'unilater', u'chines', u'project/offic', u'dressedin', u'ishallriseagain', u'towarn', u'deliber', u'immers', u'varidesk', u'engineern', u'loos', u'specialis', u'chees', u'propos', u'ground/hom', u'apologis', u'hasthina', u'cautious', u'outlining/pr', u'dlowan', u'polisci', u'conatin', u'rdean', u'football/socc', u'meaning', u'evil/viol', u'burkhaus', u'experiment', u'papth', u'bucephelu', u'posititv', u'busni', u'freakyalarm', u'purs', u'pois', u'pbase', u'famous', u'org/wiki/histori', u'arecom', u'terminl', u'animaci', u'collaps', u'neavtiv', u'divers', u'dens', u'schultem', u'inde', u'tens', u'eclips', u'insuir', u'flexabl', u'campus', u'connetc', u'incred', u'revers', u'obes', u'disproportion', u'whithin']
                     )
        tokens = list(tokens)

        for token in tokens:
            if token in d:
                d[token] += 1
            else:
                d[token] = 1

        word2index = {}

        i = 0
        for w in sorted(d, key=d.get, reverse=True):
            word2index[w] = i
            i += 1
            if i >= self.max_vocabulary_size -1:
                break

        print len(word2index)

        return Vocabulary(word2index)


class Vocabulary:
    def __init__(self, vocabulary):
        self.word2index = {}
        self.index2word = {}

        # there is special index for unknown word
        special_indexes_count = 1
        for word in vocabulary:
            self.index2word[len(self.word2index) + special_indexes_count] = word
            self.word2index[word] = len(self.word2index) + special_indexes_count

        self.sequence_width = len(vocabulary) + special_indexes_count

    def __init__(self, word2index):
        self.word2index = word2index
        self.index2word = dict((v, k) for k, v in word2index.iteritems())

        self.sequence_width = len(word2index)


    def get_index_for_word(self, word):
        if word in self.word2index:
            return self.word2index[word]
        else:
            return 0