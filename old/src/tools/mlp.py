__author__ = 'frem'

import config
import theano

from theano import tensor as T
import numpy as np
import datetime

from blocks.bricks import WEIGHT
from blocks.filter import VariableFilter
from blocks.bricks.cost import Cost
from blocks.bricks.base import application
from blocks.algorithms import GradientDescent, AdaGrad, CompositeRule, Restrict, VariableClipping, StepClipping
from blocks.graph import ComputationGraph
from blocks.model import Model
from blocks.extensions.training import TrackTheBest
from blocks.extensions.stopping import FinishIfNoImprovementAfter
from blocks.extensions import FinishAfter, Timing, Printing, ProgressBar, Timing, SimpleExtension
from blocks.extensions.saveload import Checkpoint
from blocks.extensions.monitoring import (TrainingDataMonitoring, DataStreamMonitoring)
from blocks.extras.extensions.plot import Plot
from blocks.main_loop import MainLoop
from blocks.bricks import Activation


class CorrelationCost(Cost):
    @application(outputs=["correlation"])
    def apply(self, y, y_hat):
        a = ((y - y.mean()) * (y_hat - y_hat.mean())).sum()
        b = T.sqrt(((y - y.mean()) ** 2).sum())
        c = T.sqrt(((y_hat - y_hat.mean()) ** 2).sum())

        correlation = a / (b * c)

        return correlation


class Cos_sim(object):
    def apply(self, A, B):
        def cos_sim(a, b):
            cos = T.dot(a, b) / (T.sqrt(T.dot(a, a)) * T.sqrt(T.dot(b, b)))
            return T.as_tensor_variable([cos])

        result, _ = theano.scan(cos_sim, sequences=[A, B])
        return result


def create_main_loop(model, train_stream, validation_stream, test_streams,
                     num_epochs=50, experiment_name="exp", num_epoch_after_not_improvement=20, printing=True):
    cost = model.cost
    corr = model.corr

    corr_size = model.corr_size

    step_rule = AdaGrad()

    extensions = [Timing(),
                  TrainingDataMonitoring([cost], after_epoch=True),
                  FinishAfter(after_n_epochs=num_epochs),
                  DataStreamMonitoring(
                      [cost,
                       corr,
                        # model.baseline_corr
                       ], test_streams[0][1],
                      prefix="valid", after_epoch=True),
                  DataStreamMonitoring(
                      [corr], train_stream,
                      prefix="train", after_epoch=True, before_first_epoch=False),
                  TrackTheBest("valid_cost"),
                  # FinishIfNoImprovementAfter("valid_cost_best_so_far", epochs=num_epoch_after_not_improvement),
                  # Plot(experiment_name,
                  #      channels=[['cost', 'valid_cost'],
                  #                # [test_name + '_corr' for (test_name, _) in test_streams],
                  #                # [test_name + '_baseline_corr' for (test_name, _) in test_streams],
                  #                ['train_corr', 'valid_corr', ]# 'valid_baseline_corr']
                  #                ],
                  #      after_epoch=True,
                  #      before_first_epoch=False),
                  # Checkpoint(config.model_save_dir + experiment_name),
                  # LogsToFile(config.SRC_DIRECTORY + "experiments/logs/{}.txt".format(experiment_name))
                  ]

    extensions += [DataStreamMonitoring(
                      [corr, corr_size
                       # model.baseline_corr
                       ], test_stream, prefix=test_name+"_test", after_epoch=True) for test_name, test_stream in test_streams]
    extensions += [Correlation()]

    if printing is True:
        extensions += [Printing(after_epoch=True)]

    main_loop = MainLoop(
        GradientDescent(
            cost=cost, parameters=ComputationGraph(cost).parameters,
            step_rule=step_rule, on_unused_sources='ignore'),
        data_stream=train_stream,
        model=Model(cost),
        extensions=extensions,
    )

    return main_loop


class LogsToFile(Printing):
    """log messages to the file."""

    def __init__(self, filename, **kwargs):
        self.filename = filename

        with open(self.filename, "a") as log_file:
            log_file.write("\n {} \n".format(datetime.datetime.now()))
        super(LogsToFile, self).__init__(**kwargs)

    def _print_attributes(self, attribute_tuples):
        from toolz import first

        with open(self.filename, "a") as log_file:
            items = {}
            for attr, value in sorted(attribute_tuples.items(), key=first):
                if attr.startswith("_"):
                    continue
                if type(value) is np.ndarray:
                    items[attr] = value.tolist()
                else:
                    items[attr] = value
            import json

            log_file.write("{} \n".format(json.dumps(items)))


class Correlation(Printing):
    """log messages to the file."""

    def __init__(self, **kwargs):

        kwargs.setdefault("after_training", True)
        kwargs.setdefault("before_first_epoch", True)
        kwargs.setdefault("after_epoch", True)
        super(Correlation, self).__init__(**kwargs)


    def do(self, which_callback, *args):
        log = self.main_loop.log
        self._print_attributes(log.current_row)

    def _print_attributes(self, attribute_tuples):
        from toolz import first

        corr = 0
        counts = 0

        for attr, value in sorted(attribute_tuples.items(), key=first):
            if attr.endswith("_test_corr"):
                counts += attribute_tuples[attr + "_size"]
                corr += value * attribute_tuples[attr + "_size"]

        if counts > 0:
            print "{{Corralation: {}}}".format(corr / counts)


def add_l_norms(cost, l1=0.00001, l2=0.0001):
    old_name = cost.name

    cg = ComputationGraph(cost)
    weights = VariableFilter(roles=[WEIGHT])(cg.variables)
    for w in weights:
        cost += l2 * (w ** 2).sum()
        cost += l1 * T.abs_(w).sum()

    cost.name = old_name

    return cost

