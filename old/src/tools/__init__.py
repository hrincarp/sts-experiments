__author__ = 'frem'

import numpy as np


def cos_sim(sen_0, sen_1):
    sen_0 = set(sen_0)
    sen_1 = set(sen_1)

    d = np.sqrt(len(sen_0)*len(sen_1))
    if d != 0:
        return len(sen_0 & sen_1) / d
    else:
        return 0


def mean_test_corr(info_json):
    import json
    info = json.loads(info_json)

    test_counts = {
        "answers-forums": 375,
        "answers-students": 750,
        "belief": 375,
        "headlines": 750,
        "images": 750,
    }

    mean = 0
    counts = 0
    for key, count in test_counts.iteritems():
        mean += info[key + "_corr"] * count
        counts += count

    return mean / counts