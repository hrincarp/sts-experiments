# My project's README

Zdrojové kódy k diplomovej práci

Použitie neuronových sietí pre určenie sémantickej podobnosti dvoch viet

Peter Hrinčár



Program sa nachádza v zložke sts-experiments. Zložka je rozdelená do troch častí:

* data/, kde sa nachádzajú datasety a externé embeddings

* experiments/ obsahuje skripty a výstupné súbory k všetkým experimentom

* src/ obsahuje zdrojový kód písaný v jazyku Python


Zdrojový kód je rozdelený na
* models, implementácia použitých modelov neurónových sietí • custom blocks rozšírenie knižnice blocks
* datasets implementácia datasetov, interface k sts datasetom * experiments skripty, ktoré sa spúšťajú pri experimentoch
* scripts pomocné skripty
* tools nástroje


Spustenie experimentov napr. (počet oov)
bash ./experiments/oov_counts.sh

potrebné knižnice sú v súbore ./prerequisites.sh

viac na git@bitbucket.org:hrincarp/sts-experiments.git