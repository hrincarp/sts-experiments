echo "installing prerequisites ..."

# Install Theano+Blocks and their dependencies (remove --user to install for all users (requires root privileges))
pip install git+http://github.com/mila-udem/blocks.git@3db89c000d0c159d6d056b5f488a84ec1b6e22fe -r https://raw.githubusercontent.com/mila-udem/blocks/master/requirements.txt --user
# nltk tokenizer + punkt corpus
pip install nltk --user
python -m nltk.downloader punkt
python -m nltk.downloader stopwords
python -m nltk.downloader wordnet