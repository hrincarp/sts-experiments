from blocks.bricks import Initializable
from blocks.bricks import Linear
from blocks.bricks import Tanh
from blocks.bricks import application
from blocks.bricks.parallel import Fork
from blocks.bricks.recurrent import GatedRecurrent, LSTM, Bidirectional
from blocks.initialization import IsotropicGaussian, Constant, Orthogonal
from picklable_itertools.extras import equizip
from theano import tensor
from toolz import merge

from custom_blocks.batch_normalized_rnns import BNGatedRecurrent, BNLSTM


def create_bidi_encoder(name, embedding_dims, hidden_states, recurrent_type='GRU'):
    encoder = BidirectionalEncoder(embedding_dims, hidden_states, name=name, recurrent_type=recurrent_type)

    # Set up the initialization methods
    weight_scale = 0.1
    encoder.weights_init = IsotropicGaussian(
        weight_scale)
    encoder.biases_init = Constant(0)

    encoder.push_initialization_config()
    encoder.bidir.prototype.weights_init = Orthogonal()
    encoder.initialize()

    return encoder

class BidirectionalFromDict(Bidirectional):
    """ Bidirectional RNN that takes inputs in form of a dict. """

    @application
    def apply(self, forward_dict, backward_dict):
        """Applies forward and backward networks and concatenates outputs."""
        forward = self.children[0].apply(as_list=True, **forward_dict)
        backward = [x[::-1] for x in
                    self.children[1].apply(reverse=True, as_list=True,
                                           **backward_dict)]
        # This eliminated the extra output that LSTM has in addition to GRU
        forward = [forward[0]]
        backward = [backward[0]]
        return [tensor.concatenate([f, b], axis=2)
                for f, b in equizip(forward, backward)]


class BidirectionalEncoder(Initializable):

    def __init__(self, embedding_dim, state_dim, recurrent_type='GRU', **kwargs):
        super(BidirectionalEncoder, self).__init__(**kwargs)
        # Dimension of the word embeddings taken as input
        self.embedding_dim = embedding_dim
        # Hidden state dimension
        self.state_dim = state_dim

        common_params = {'activation': Tanh(), 'dim': state_dim}
        if recurrent_type == 'GRU':
            recurrent_prototype = GatedRecurrent(**common_params)
        elif recurrent_type == 'BNGRU':
            recurrent_prototype = BNGatedRecurrent(**common_params)
        elif recurrent_type == 'LSTM':
            recurrent_prototype = LSTM(**common_params)
        elif recurrent_type == 'BNLSTM':
            recurrent_prototype = BNLSTM(**common_params)

        # The bidir GRU
        self.bidir = BidirectionalFromDict(recurrent_prototype)
        # Forks to administer the inputs of GRU gates
        self.fwd_fork = Fork(
            [name for name in self.bidir.prototype.apply.sequences
             if name != 'mask'], prototype=Linear(), name='fwd_fork')
        self.back_fork = Fork(
            [name for name in self.bidir.prototype.apply.sequences
             if name != 'mask'], prototype=Linear(), name='back_fork')

        self.children = [self.bidir,
                         self.fwd_fork, self.back_fork]

    def _push_allocation_config(self):
        self.fwd_fork.input_dim = self.embedding_dim
        self.fwd_fork.output_dims = [self.bidir.children[0].get_dim(name)
                                     for name in self.fwd_fork.output_names]
        self.back_fork.input_dim = self.embedding_dim
        self.back_fork.output_dims = [self.bidir.children[1].get_dim(name)
                                      for name in self.back_fork.output_names]

    @application(inputs=['source_sentence_tbf', 'source_sentence_mask_tb'],
                 outputs=['representation'])
    def apply(self, source_sentence_tbf, source_sentence_mask_tb=None):

        representation_tbf = self.bidir.apply(
            merge(self.fwd_fork.apply(source_sentence_tbf, as_dict=True),
                  {'mask': source_sentence_mask_tb}),
            merge(self.back_fork.apply(source_sentence_tbf, as_dict=True),
                  {'mask': source_sentence_mask_tb})
        )
        return representation_tbf


class BidirectionalEncoder(Initializable):
    """ Bidirectional GRU encoder. """

    def __init__(self, embedding_dim, state_dim, recurrent_type='GRU', **kwargs):
        super(BidirectionalEncoder, self).__init__(**kwargs)
        # Dimension of the word embeddings taken as input
        self.embedding_dim = embedding_dim
        # Hidden state dimension
        self.state_dim = state_dim

        common_params = {'activation': Tanh(), 'dim': state_dim}
        if recurrent_type == 'GRU':
            recurrent_prototype = GatedRecurrent(**common_params)
        elif recurrent_type == 'BNGRU':
            recurrent_prototype = BNGatedRecurrent(**common_params)
        elif recurrent_type == 'LSTM':
            recurrent_prototype = LSTM(**common_params)
        elif recurrent_type == 'BNLSTM':
            recurrent_prototype = BNLSTM(**common_params)

        # The bidir GRU
        self.bidir = BidirectionalFromDict(recurrent_prototype)
        # Forks to administer the inputs of GRU gates
        self.fwd_fork = Fork(
            [name for name in self.bidir.prototype.apply.sequences
             if name != 'mask'], prototype=Linear(), name='fwd_fork')
        self.back_fork = Fork(
            [name for name in self.bidir.prototype.apply.sequences
             if name != 'mask'], prototype=Linear(), name='back_fork')

        self.children = [self.bidir,
                         self.fwd_fork, self.back_fork]

    def _push_allocation_config(self):
        self.fwd_fork.input_dim = self.embedding_dim
        self.fwd_fork.output_dims = [self.bidir.children[0].get_dim(name)
                                     for name in self.fwd_fork.output_names]
        self.back_fork.input_dim = self.embedding_dim
        self.back_fork.output_dims = [self.bidir.children[1].get_dim(name)
                                      for name in self.back_fork.output_names]

    @application(inputs=['source_sentence_tbf', 'source_sentence_mask_tb'],
                 outputs=['representation'])
    def apply(self, source_sentence_tbf, source_sentence_mask_tb=None):

        representation_tbf = self.bidir.apply(
            merge(self.fwd_fork.apply(source_sentence_tbf, as_dict=True),
                  {'mask': source_sentence_mask_tb}),
            merge(self.back_fork.apply(source_sentence_tbf, as_dict=True),
                  {'mask': source_sentence_mask_tb})
        )
        return representation_tbf
