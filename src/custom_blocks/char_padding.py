from fuel.transformers import Transformer, Padding
import numpy as np


class VocabFromChars(Transformer):
    """
    Creates local vocabulary and put indexes instead of words
    """

    def __init__(self, data_stream, words_sources, **kwargs):
        if data_stream.produces_examples:
            raise ValueError('the wrapped data stream must produce batches of '
                             'examples, not examples')
        super(VocabFromChars, self).__init__(
                data_stream, produces_examples=False, **kwargs)

        self.words_sources = words_sources

    @property
    def sources(self):
        sources = []
        for source in self.data_stream.sources:
            sources.append(source)
            if source in self.words_sources:
                sources.append(source + '_local_indx')

        sources.append("vocab")
        return tuple(sources)

    def transform_batch(self, batch):
        batch_with_vocab = []

        vocab = {}

        def get_word_index(word):
            if word not in vocab:
                vocab[word] = len(vocab)

            return vocab[word]

        def translate_word(word, max_lenght=30):
            # max 255 index
            chars = np.array([ord(ch) % 255 for ch in word[:max_lenght]], dtype=int)
            return chars

        for i, (source, source_batch) in enumerate(
                zip(self.data_stream.sources, batch)):

            batch_with_vocab.append(source_batch)

            if source not in self.words_sources:
                continue

            sample_vocab_i = [np.array([get_word_index(word) for word in sample], dtype=int) for sample in source_batch]

            batch_with_vocab.append(sample_vocab_i)

        vocab_source = []
        for word in sorted(vocab, key=vocab.get):
            vocab_source += [translate_word(word)]

        batch_with_vocab.append(vocab_source)

        return tuple(batch_with_vocab)


class CharPadding(Padding):
    """Adds padding to variable-length sequences of characters (words should be already padded).

    Parameters
    ----------
    data_stream : :class:`AbstractDataStream` instance
        The data stream to wrap
    mask_sources : tuple of strings, optional
        The sources for which we need to add a mask. If not provided, a
        mask will be created for all data sources
    mask_dtype: str, optional
        data type of masks. If not provided, floatX from config will
        be used.

    """

    @property
    def sources(self):
        sources = []
        for source in self.data_stream.sources:
            sources.append(source)
            if source in self.mask_sources:
                sources.append(source + '_ch_mask')
        return tuple(sources)

    def transform_batch(self, batch):
        batch_with_masks = []
        for i, (source, source_batch) in enumerate(
                zip(self.data_stream.sources, batch)):
            if source not in self.mask_sources:
                batch_with_masks.append(source_batch)
                continue

            dtype = np.asarray(source_batch[0][0]).dtype

            def get_max_length():
                max_length = 0
                for sample in source_batch:
                    for word in sample:
                        if len(np.asarray(word).shape) == 0:
                            continue

                    shapes = [np.asarray(word).shape for word in np.array(sample)]
                    lengths = [shape[0] for shape in shapes if len(shape) > 0]
                    max_length = max([max_length] + lengths)
                return max_length

            max_sequence_length = get_max_length()

            padded_batch = np.zeros(source_batch.shape + (max_sequence_length,), dtype=dtype)

            for s, sample in enumerate(source_batch):
                for w, word in enumerate(sample):
                    if len(np.asarray(word).shape) == 0:
                        continue
                    padded_batch[s, w, :len(word)] = np.asarray(word)

            batch_with_masks.append(padded_batch)

            mask = np.zeros(source_batch.shape + (max_sequence_length,), dtype=self.mask_dtype)

            for i, sample in enumerate(source_batch):
                for j, word in enumerate(sample):
                    if len(np.asarray(word).shape) == 0:
                        continue
                    mask[i, j, :len(word)] = 1
            batch_with_masks.append(mask)
        return tuple(batch_with_masks)
