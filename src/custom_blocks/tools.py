from theano import tensor as tt, theano


def cos_sim(a, b):
    cos = tt.dot(a, b) / (tt.sqrt(tt.dot(a, a)) * tt.sqrt(tt.dot(b, b)))
    return tt.as_tensor_variable([cos])


class CosSim(object):
    def apply(self, a_b, b_b):
        result, _ = theano.scan(cos_sim, sequences=[a_b, b_b])
        return result
