import theano.tensor as tt
from blocks.bricks import Initializable, application, Rectifier
from custom_blocks.convolution.convolution_1d import Convolutional1D
from custom_blocks.convolution.max_over_time import MaxOverTime


class Conv1DMaxOverTime(Initializable):
    """ Convolution with non linearity followed by max pooling over time.

    Parameters
    ----------
    filter_length:
        length of the convolution filter
    filters_num:
        number of filters
    input_dim:
        dimension of vectors used as input for convolution
    """
    def __init__(self, filter_length, filters_num, input_dim, weights_init=None, biases_init=None, activation=None,
                 **kwargs):
        super(Conv1DMaxOverTime, self).__init__(**kwargs)

        self.convolution1d = Conv1DMasked(
                filter_length,
                filters_num,
                input_dim,
                weights_init=weights_init,
                biases_init=biases_init,
                activation=activation)

        self.max = MaxOverTime()
        self.children = [self.convolution1d, self.max]

    @application(inputs=['input', 'mask'], outputs=['output'])
    def apply(self, input, mask):
        """Apply the max pooling over time transformation.

        Parameters
        ----------
        input : :class:`~tensor.TensorVariable`
            An tensor with dimensions (time X batch X features)

        Returns
        -------
        output : :class:`~tensor.TensorVariable`
            A tensor with shape (batch X features)
        """

        # compute result of 1d convolution
        conved, x_mask_padded = self.convolution1d.apply(input, mask)

        # compute maximum over each filter's activation
        max_pool_result = self.max.apply(input=conved, mask=x_mask_padded)

        return max_pool_result

    def get_dim(self, name):
        if name == 'mask':
            return 0
        return 0

    def _initialize(self):
        self.convolution1d.initialize()


class Conv1DMasked(Initializable):
    """ Convolution with non linearity. Returns both a result of convolution and new padded mask.

    Parameters
    ----------
    filter_length:
        length of the convolution filter
    filters_num:
        number of filters
    input_dim:
        dimension of vectors used as input for convolution
    """

    def __init__(self, filter_length, filters_num, input_dim, weights_init=None, biases_init=None, activation=None,
                 **kwargs):

        super(Conv1DMasked, self).__init__(**kwargs)

        self.convolution1d = Convolutional1D(
                filter_size=(filter_length, input_dim),
                num_filters=filters_num,
                weights_init=weights_init,
                biases_init=biases_init)

        self.activation = activation
        if not activation:
            self.activation = Rectifier()

        self.children = [self.convolution1d, self.activation]

    @application(inputs=['input', 'mask'], outputs=['output', 'mask_padded'])
    def apply(self, input, mask):
        """Apply the max pooling over time transformation.

        Parameters
        ----------
        input : :class:`~tensor.TensorVariable`
            An tensor with dimensions (time X batch X features)

        Returns
        -------
        output : :class:`~tensor.TensorVariable`
            A tensor with shape (batch X features)
        """

        # compute result of 1d convolution
        conved_pre_act = self.convolution1d.apply(input)

        # apply activation function
        conved = self.activation.apply(conved_pre_act)

        # extend mask since input is padded
        mask_padding = tt.alloc(1, self.convolution1d.filter_length - 1, mask.shape[1])
        x_mask_padded = tt.concatenate((mask_padding, mask), axis=0)

        return conved, x_mask_padded

    def get_dim(self, name):
        if name == 'mask':
            return 0
        return 0

    def _initialize(self):
        self.convolution1d.initialize()


class MultiConv1DMaxOverTime(Initializable):
    """ Convolution with non linearity followed by max pooling over time.

    Parameters
    ----------
    filter_length:
        length of the convolution filters
    filters_num:
        number of filters
    input_dim:
        dimension of vectors used as input for convolution
    """

    def __init__(self, filter_lengths, filters_nums, input_dim, weights_init=None, biases_init=None, activation=None,
                 **kwargs):
        super(MultiConv1DMaxOverTime, self).__init__(**kwargs)
        self.filter_lengths = filter_lengths
        self.filters_nums = filters_nums
        self.input_dim = input_dim

        conv_bricks = []
        for filters, length in zip(filters_nums, filter_lengths):
            convMaxOverTime = Conv1DMaxOverTime(length, filters, input_dim=input_dim,
                                                weights_init=weights_init,
                                                biases_init=biases_init, activation=activation,
                                                name="conv" + str(filters) + "x" + str(length))
            conv_bricks.append(convMaxOverTime)

        self.children = conv_bricks

    @application(inputs=['input', 'mask'], outputs=['output'])
    def apply(self, input, mask):

        conv_results = []
        for conv_and_max_brick in self.children:
            max_pool_result = conv_and_max_brick.apply(input=input, mask=mask)
            conv_results.append(max_pool_result)

        # concatenate result of all convolutions
        max_pool_result = tt.concatenate(conv_results, axis=1)

        return max_pool_result

    def get_dim(self, name):
        if name == "output":
            return sum(self.filters_nums)
        else:
            return super(MultiConv1DMaxOverTime, self).get_dim(name)
