import theano.tensor as tt
from blocks.bricks import application, Feedforward


class MaxOverTime(Feedforward):
    def __init__(self, **kwargs):
        super(MaxOverTime, self).__init__(**kwargs)

    @application(inputs=['input', 'mask'], outputs=['output'])
    def apply(self, input, mask):
        """Apply the max pooling over time transformation.

        Parameters
        ----------
        input : :class:`~tensor.TensorVariable`
            A tensor with dimensions (time X batch X features).
        mask : :class:`~tensor.TensorVariable`
            A tensor with dimensions (time X batch).

        Returns
        -------
        output : :class:`~tensor.TensorVariable`
            A tensor with dimension (batch X features) where features store the maximum value over time.

        """

        # TODO maybe instead of computing min we can use NaN or some other value
        # get minimum value from all elements
        min_val = tt.min(input)

        # create matrix with min values everywhere
        masking_value_matrix = tt.fill(input, min_val)

        mask_3d = mask.dimshuffle(0, 1, 'x')

        # use mask to put -inf everywhere where the mask has 0
        masked = input * mask_3d + (1 - mask_3d) * masking_value_matrix

        # get maximum over time
        output = tt.max(masked, axis=0)
        return output

    def get_dim(self, name):
        if name == 'mask':
            return 0
        return 0
