import numpy
from blocks.extensions.monitoring import DataStreamMonitoring
import json
import os

class FileDataStreamMonitoring(DataStreamMonitoring):

    PREFIX_SEPARATOR = '_'

    def __init__(self, output_directory, **kwargs):
        self.output_directory = output_directory

        if not os.path.exists(output_directory):
            os.makedirs(output_directory)

        super(FileDataStreamMonitoring, self).__init__(**kwargs)

    def do(self, callback_name, *args):
        value_dict = self._evaluator.evaluate(self.data_stream)

        for key in value_dict:
            values = numpy.array(value_dict[key]).tolist()

            key = str(key)
            file_path = os.path.join(self.output_directory, key)

            with open(file_path, 'w') as fp:
                try:
                    for value in values:
                        fp.write("{}\n".format(value))
                except:
                    fp.write("{}\n".format(values))

