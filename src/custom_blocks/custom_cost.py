from blocks.bricks.base import application
from blocks.bricks.cost import Cost
from theano import tensor as tt


class CorrelationCost(Cost):
    @application(outputs=["correlation"])
    def apply(self, y, y_hat):
        a = ((y - y.mean()) * (y_hat - y_hat.mean())).sum()
        b = tt.sqrt(((y - y.mean()) ** 2).sum())
        c = tt.sqrt(((y_hat - y_hat.mean()) ** 2).sum())

        correlation = a / (b * c)

        return correlation
