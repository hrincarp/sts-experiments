import numpy
from blocks.bricks import Initializable, Logistic, Tanh
from blocks.bricks.base import application, lazy
from blocks.bricks.recurrent import BaseRecurrent, recurrent
from blocks.roles import add_role, WEIGHT, INITIAL_STATE
from blocks.utils import (shared_floatx_nans, shared_floatx_zeros)
from theano import tensor
from theano.tensor.nnet.bn import batch_normalization


class BNGatedRecurrent(BaseRecurrent, Initializable):
    u""" Batch-normalized Gated recurrent neural network.
    Based on a copy-and-paste of GatedRecurrent from blocks.bricks.recurrent

    Gated recurrent neural network (GRNN) as introduced in [CvMG14]_. Every
    unit of a GRNN is equipped with update and reset gates that facilitate
    better gradient propagation.

    Parameters
    ----------
    dim : int
        The dimension of the hidden state.
    activation : :class:`.Brick` or None
        The brick to apply as activation. If ``None`` a
        :class:`.Tanh` brick is used.
    gate_activation : :class:`.Brick` or None
        The brick to apply as activation for gates. If ``None`` a
        :class:`.Logistic` brick is used.


    """

    @lazy(allocation=['dim'])
    def __init__(self, dim, activation=None, gate_activation=None, epsilon=1e-5,
                 **kwargs):
        self.dim = dim

        if not activation:
            activation = Tanh()
        if not gate_activation:
            gate_activation = Logistic()
        self.activation = activation
        self.gate_activation = gate_activation
        self.epsilon = epsilon

        children = [activation, gate_activation] + kwargs.get('children', [])
        super(BNGatedRecurrent, self).__init__(children=children, **kwargs)

    @property
    def state_to_state(self):
        return self.parameters[0]

    @property
    def state_to_gates(self):
        return self.parameters[1]

    @property
    def bn_gamma_input(self):
        return self.parameters[2]

    @property
    def bn_gamma_gate_input(self):
        return self.parameters[3]

    def get_dim(self, name):
        if name == 'mask':
            return 0
        if name in ['inputs', 'states']:
            return self.dim
        if name == 'gate_inputs':
            return 2 * self.dim
        return super(BNGatedRecurrent, self).get_dim(name)

    def _allocate(self):
        self.parameters.append(shared_floatx_nans((self.dim, self.dim),
                                                  name='state_to_state'))
        self.parameters.append(shared_floatx_nans((self.dim, 2 * self.dim),
                                                  name='state_to_gates'))
        self.parameters.append(shared_floatx_nans((self.dim,),
                                                  name='bn_gamma_input'))
        self.parameters.append(shared_floatx_nans((2 * self.dim,),
                                                  name='bn_gamma_gate_input'))
        self.parameters.append(shared_floatx_zeros((self.dim,),
                                                   name="initial_state"))
        for i in range(4):
            if self.parameters[i]:
                add_role(self.parameters[i], WEIGHT)
        add_role(self.parameters[4], INITIAL_STATE)

    def _initialize(self):
        self.weights_init.initialize(self.state_to_state, self.rng)
        self.weights_init.initialize(self.bn_gamma_input, self.rng)
        self.weights_init.initialize(self.bn_gamma_gate_input, self.rng)

        state_to_update = self.weights_init.generate(
            self.rng, (self.dim, self.dim))
        state_to_reset = self.weights_init.generate(
            self.rng, (self.dim, self.dim))
        self.state_to_gates.set_value(
            numpy.hstack([state_to_update, state_to_reset]))

    @recurrent(sequences=['mask', 'inputs', 'gate_inputs'],
               states=['states'], outputs=['states'], contexts=[])
    def apply(self, inputs, gate_inputs, states, mask=None):
        """Apply the gated recurrent transition.

        Parameters
        ----------
        states : :class:`~tensor.TensorVariable`
            The 2 dimensional matrix of current states in the shape
            (batch_size, dim). Required for `one_step` usage.
        inputs : :class:`~tensor.TensorVariable`
            The 2 dimensional matrix of inputs in the shape (batch_size,
            dim)
        gate_inputs : :class:`~tensor.TensorVariable`
            The 2 dimensional matrix of inputs to the gates in the
            shape (batch_size, 2 * dim).
        mask : :class:`~tensor.TensorVariable`
            A 1D binary array in the shape (batch,) which is 1 if there is
            data available, 0 if not. Assumed to be 1-s only if not given.

        Returns
        -------
        output : :class:`~tensor.TensorVariable`
            Next states of the network.

        """
        normalized_inputs = batch_normalization(inputs=inputs, gamma=self.bn_gamma_input,
                                                beta=tensor.zeros_like(inputs),
                                                mean=inputs.mean(axis=0),
                                                std=tensor.sqrt(inputs.var(axis=0) + self.epsilon))
        normalized_gate_inputs = batch_normalization(gate_inputs, self.bn_gamma_gate_input,
                                                     tensor.zeros_like(gate_inputs),
                                                     gate_inputs.mean(axis=0),
                                                     tensor.sqrt(gate_inputs.var(axis=0) + self.epsilon))

        gate_values = self.gate_activation.apply(
            states.dot(self.state_to_gates) + normalized_gate_inputs)
        update_values = gate_values[:, :self.dim]
        reset_values = gate_values[:, self.dim:]
        states_reset = states * reset_values
        next_states = self.activation.apply(
            states_reset.dot(self.state_to_state) + normalized_inputs)
        next_states = (next_states * update_values +
                       states * (1 - update_values))
        if mask:
            next_states = (mask[:, None] * next_states +
                           (1 - mask[:, None]) * states)
        return next_states

    @application(outputs=apply.states)
    def initial_states(self, batch_size, *args, **kwargs):
        return [tensor.repeat(self.parameters[4][None, :], batch_size, 0)]


class BNLSTM(BaseRecurrent, Initializable):
    u""" Batch Normalized Long Short Term Memory.
    Based on a copy-and-paste of LSTM from blocks.bricks.recurrent.

    """

    @lazy(allocation=['dim'])
    def __init__(self, dim, activation=None, epsilon=1e-5, **kwargs):
        self.dim = dim
        self.epsilon = epsilon

        if not activation:
            activation = Tanh()
        children = [activation] + kwargs.get('children', [])
        super(BNLSTM, self).__init__(children=children, **kwargs)

    def get_dim(self, name):
        if name == 'inputs':
            return self.dim * 4
        if name in ['states', 'cells']:
            return self.dim
        if name == 'mask':
            return 0
        return super(BNLSTM, self).get_dim(name)

    def _allocate(self):
        self.W_state = shared_floatx_nans((self.dim, 4 * self.dim),
                                          name='W_state')
        self.W_cell_to_in = shared_floatx_nans((self.dim,),
                                               name='W_cell_to_in')
        self.W_cell_to_forget = shared_floatx_nans((self.dim,),
                                                   name='W_cell_to_forget')
        self.W_cell_to_out = shared_floatx_nans((self.dim,),
                                                name='W_cell_to_out')
        self.bn_gamma_inputs = shared_floatx_nans((4 * self.dim,),
                                                  name='bn_gamma_inputs')
        self.bn_gamma_states = shared_floatx_nans((self.dim,),
                                                  name='bn_gamma_states')
        # The underscore is required to prevent collision with
        # the `initial_state` application method
        self.initial_state_ = shared_floatx_zeros((self.dim,),
                                                  name="initial_state")
        self.initial_cells = shared_floatx_zeros((self.dim,),
                                                 name="initial_cells")
        add_role(self.W_state, WEIGHT)
        add_role(self.W_cell_to_in, WEIGHT)
        add_role(self.W_cell_to_forget, WEIGHT)
        add_role(self.W_cell_to_out, WEIGHT)
        add_role(self.bn_gamma_inputs, WEIGHT)
        add_role(self.bn_gamma_states, WEIGHT)
        add_role(self.initial_state_, INITIAL_STATE)
        add_role(self.initial_cells, INITIAL_STATE)

        self.parameters = [
            self.W_state, self.W_cell_to_in, self.W_cell_to_forget,
            self.W_cell_to_out, self.bn_gamma_inputs, self.bn_gamma_states, self.initial_state_, self.initial_cells]

    def _initialize(self):
        for weights in self.parameters[:6]:
            self.weights_init.initialize(weights, self.rng)

    @recurrent(sequences=['inputs', 'mask'], states=['states', 'cells'],
               contexts=[], outputs=['states', 'cells'])
    def apply(self, inputs, states, cells, mask=None):
        """Apply the Long Short Term Memory transition.

        Parameters
        ----------
        states : :class:`~tensor.TensorVariable`
            The 2 dimensional matrix of current states in the shape
            (batch_size, features). Required for `one_step` usage.
        cells : :class:`~tensor.TensorVariable`
            The 2 dimensional matrix of current cells in the shape
            (batch_size, features). Required for `one_step` usage.
        inputs : :class:`~tensor.TensorVariable`
            The 2 dimensional matrix of inputs in the shape (batch_size,
            features * 4). The `inputs` needs to be four times the
            dimension of the LSTM brick to insure each four gates receive
            different transformations of the input. See [Grav13]_
            equations 7 to 10 for more details. The `inputs` are then split
            in this order: Input gates, forget gates, cells and output
            gates.
        mask : :class:`~tensor.TensorVariable`
            A 1D binary array in the shape (batch,) which is 1 if there is
            data available, 0 if not. Assumed to be 1-s only if not given.

        .. [Grav13] Graves, Alex, *Generating sequences with recurrent
            neural networks*, arXiv preprint arXiv:1308.0850 (2013).

        Returns
        -------
        states : :class:`~tensor.TensorVariable`
            Next states of the network.
        cells : :class:`~tensor.TensorVariable`
            Next cell activations of the network.

        """

        def slice_last(x, no):
            return x[:, no * self.dim: (no + 1) * self.dim]

        normalized_inputs = batch_normalization(inputs=inputs, gamma=self.bn_gamma_inputs,
                                                beta=tensor.zeros_like(inputs),
                                                mean=inputs.mean(axis=0),
                                                std=tensor.sqrt(inputs.var(axis=0) + self.epsilon))

        normalized_states = batch_normalization(states, self.bn_gamma_states, tensor.zeros_like(states),
                                                states.mean(axis=0),
                                                tensor.sqrt(states.var(axis=0) + self.epsilon))

        nonlinearity = self.children[0].apply

        activation = tensor.dot(normalized_states, self.W_state) + normalized_inputs
        in_gate = tensor.nnet.sigmoid(slice_last(activation, 0) +
                                      cells * self.W_cell_to_in)
        forget_gate = tensor.nnet.sigmoid(slice_last(activation, 1) +
                                          cells * self.W_cell_to_forget)
        next_cells = (forget_gate * cells +
                      in_gate * nonlinearity(slice_last(activation, 2)))
        out_gate = tensor.nnet.sigmoid(slice_last(activation, 3) +
                                       next_cells * self.W_cell_to_out)
        next_states = out_gate * nonlinearity(next_cells)

        if mask:
            next_states = (mask[:, None] * next_states +
                           (1 - mask[:, None]) * normalized_states)
            next_cells = (mask[:, None] * next_cells +
                          (1 - mask[:, None]) * cells)

        return next_states, next_cells

    @application(outputs=apply.states)
    def initial_states(self, batch_size, *args, **kwargs):
        return [tensor.repeat(self.initial_state_[None, :], batch_size, 0),
                tensor.repeat(self.initial_cells[None, :], batch_size, 0)]
