from blocks.model import Model


def none_filter_fn(o):
    return o is not None


class MultiOutputModel(Model):
    """
    Model with multiple outputs. Only the first one is used as optimization objective.
    """
    def __init__(self, outputs):
        self.orig_outputs = outputs
        # filter out None elements
        filtered = filter(none_filter_fn, outputs)
        super(MultiOutputModel, self).__init__(filtered)

    def get_objective(self):
        """Return the output variable, if there is a single one.

        If there is only one output variable, it is a reasonable default
        setting to assume that it is the optimization objective.

        """
        return self.outputs[0]
