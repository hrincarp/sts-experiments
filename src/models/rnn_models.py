import numpy
import theano
from blocks.bricks import Linear, Rectifier, Softmax, MLP, Identity, Logistic
from blocks.bricks import Tanh
from blocks.bricks.lookup import LookupTable
from blocks.bricks.recurrent import LSTM, SimpleRecurrent, Bidirectional, GatedRecurrent
from blocks.graph import apply_dropout, ComputationGraph
from blocks.initialization import Uniform, IsotropicGaussian, Constant
from theano import tensor
from theano.printing import Print

from custom_blocks.convolution.conv1d_max import MultiConv1DMaxOverTime
from custom_blocks.encoder import create_bidi_encoder
from custom_blocks.tools import CosSim
from models.basic_models import create_lookup


def lstm_similarity(si1_btf, si1_mask_bt, si2_btf, si2_mask_bt, symbols_num, embeddings_dim, embeddings):
    si1_bt = si1_btf[:, :, 0]
    si2_bt = si2_btf[:, :, 0]

    encoder = LSTM(dim=embeddings_dim, activation=Tanh(), weights_init=IsotropicGaussian(0.1))
    encoder.initialize()

    x_to_h = Linear(name='x_to_h',
                    input_dim=embeddings_dim,
                    output_dim=4 * embeddings_dim, weights_init=IsotropicGaussian(0.01), use_bias=False)
    x_to_h.initialize()

    lookup = create_lookup(embeddings_dim, symbols_num, embeddings=embeddings)
    lookup.initialize()

    s1 = x_to_h.apply(lookup.apply(si1_bt.T))
    _, ps1 = encoder.apply(s1, mask=si1_mask_bt.T)

    s2_lookup = lookup.apply(si2_bt.T)
    s2 = x_to_h.apply(s2_lookup)
    _, ps2 = encoder.apply(s2, mask=si2_mask_bt.T)

    v1 = ps1[-1, :, :]
    v2 = ps2[-1, :, :]

    model = CosSim().apply(v1, v2)

    return model



def bilstm_similarity(si1_btf, si1_mask_bt, si2_btf, si2_mask_bt, symbols_num, embeddings_dim, embeddings):
    si1_bt = si1_btf[:, :, 0]
    si2_bt = si2_btf[:, :, 0]

    encoder = LSTM(dim=embeddings_dim, activation=Tanh(), weights_init=IsotropicGaussian(0.1))

    bi = Bidirectional(encoder)
    bi.initialize()

    encoder.initialize()

    x_to_h = Linear(name='x_to_h',
                    input_dim=embeddings_dim,
                    output_dim=4 * embeddings_dim, weights_init=IsotropicGaussian(0.01), use_bias=False)
    x_to_h.initialize()

    lookup = create_lookup(embeddings_dim, symbols_num, embeddings=embeddings)
    lookup.initialize()

    s1 = x_to_h.apply(lookup.apply(si1_bt.T))
    _, ps1 = bi.apply(s1, mask=si1_mask_bt.T, )

    s2_lookup = lookup.apply(si2_bt.T)
    s2 = x_to_h.apply(s2_lookup)
    _, ps2 = bi.apply(s2, mask=si2_mask_bt.T)

    v1 = ps1[-1, :, :]
    v2 = ps2[-1, :, :]

    model = CosSim().apply(v1, v2)

    return model


def rnn_similarity(si1_btf, si1_mask_bt, si2_btf, si2_mask_bt, symbols_num, embeddings_dim, embeddings):
    si1_bt = si1_btf[:, :, 0]
    si2_bt = si2_btf[:, :, 0]

    encoder = SimpleRecurrent(dim=embeddings_dim, activation=Tanh(), weights_init=IsotropicGaussian(0.1))
    encoder.initialize()

    x_to_h = Linear(name='x_to_h',
                    input_dim=embeddings_dim,
                    output_dim=embeddings_dim, weights_init=IsotropicGaussian(0.01), use_bias=False)
    x_to_h.initialize()

    lookup = create_lookup(embeddings_dim, symbols_num, embeddings=embeddings)
    lookup.initialize()

    s1 = x_to_h.apply(lookup.apply(si1_bt.T))
    ps1 = encoder.apply(s1, mask=si1_mask_bt.T)

    s2_lookup = lookup.apply(si2_bt.T)
    s2 = x_to_h.apply(s2_lookup)
    ps2 = encoder.apply(s2, mask=si2_mask_bt.T)

    v1 = ps1[-1, :, :]
    v2 = ps2[-1, :, :]

    model = CosSim().apply(v1, v2)

    return model

