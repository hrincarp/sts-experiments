import numpy
import theano
from blocks.bricks import Linear, Rectifier, Softmax, MLP, Identity, Logistic
from blocks.bricks import Tanh
from blocks.bricks.lookup import LookupTable
from blocks.bricks.recurrent import LSTM, SimpleRecurrent, Bidirectional, GatedRecurrent
from blocks.graph import apply_dropout, ComputationGraph
from blocks.initialization import Uniform, IsotropicGaussian, Constant
from theano import tensor
from theano.printing import Print

from custom_blocks.convolution.conv1d_max import MultiConv1DMaxOverTime
from custom_blocks.encoder import create_bidi_encoder
from custom_blocks.tools import CosSim
from models.basic_models import create_lookup


def cbow_similarity_base(si1_btf, si1_mask_bt, si2_btf, si2_mask_bt, symbols_num, embeddings_dim, embeddings):
    si1_bt = si1_btf[:, :, 0]
    si2_bt = si2_btf[:, :, 0]

    # networks parameters
    lookup = create_lookup(embeddings_dim, symbols_num, embeddings=embeddings)
    lookup.initialize()

    si1_btf = lookup.apply(si1_bt) * si1_mask_bt[:, :, None]
    si2_btf = lookup.apply(si2_bt) * si2_mask_bt[:, :, None]

    return si1_btf, si2_btf


def sum_cbow_similarity(si1_btf, si1_mask_bt, si2_btf, si2_mask_bt, symbols_num, embeddings_dim, embeddings):
    si1_btf, si2_btf = cbow_similarity_base(si1_btf, si1_mask_bt, si2_btf, si2_mask_bt, symbols_num, embeddings_dim,
                                            embeddings)

    a = si1_btf.sum(axis=1)
    b = si2_btf.sum(axis=1)

    y_hat = CosSim().apply(a, b)

    return y_hat


def max_cbow_similarity(si1_btf, si1_mask_bt, si2_btf, si2_mask_bt, symbols_num, embeddings_dim, embeddings):
    si1_btf, si2_btf = cbow_similarity_base(si1_btf, si1_mask_bt, si2_btf, si2_mask_bt, symbols_num, embeddings_dim,
                                            embeddings)

    a = si1_btf.max(axis=1)
    b = si2_btf.max(axis=1)

    y_hat = CosSim().apply(a, b)

    return y_hat


def min_cbow_similarity(si1_btf, si1_mask_bt, si2_btf, si2_mask_bt, symbols_num, embeddings_dim, embeddings):
    si1_btf, si2_btf = cbow_similarity_base(si1_btf, si1_mask_bt, si2_btf, si2_mask_bt, symbols_num, embeddings_dim,
                                            embeddings)

    a = si1_btf.min(axis=1)
    b = si2_btf.min(axis=1)

    y_hat = CosSim().apply(a, b)

    return y_hat


def mean_cbow_similarity(si1_btf, si1_mask_bt, si2_btf, si2_mask_bt, symbols_num, embeddings_dim, embeddings):
    si1_btf, si2_btf = cbow_similarity_base(si1_btf, si1_mask_bt, si2_btf, si2_mask_bt, symbols_num, embeddings_dim,
                                            embeddings)

    a = si1_btf.mean(axis=1)
    b = si2_btf.mean(axis=1)

    y_hat = CosSim().apply(a, b)

    return y_hat
