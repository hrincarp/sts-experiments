import theano
from blocks.bricks import Linear, Rectifier, Tanh
from blocks.bricks.lookup import LookupTable
from blocks.initialization import Uniform, Constant, IsotropicGaussian
from theano import tensor as tt
from theano.printing import Print

from custom_blocks.convolution.conv1d_max import MultiConv1DMaxOverTime
from custom_blocks.tools import CosSim
from datasets.string2token import String2IndexesHash
from datasets.sts_dataset import STSDataset
from experiments.similarity_experiment_base import SimilarityExperimentBase
from models.basic_models import cbow_similarity, cbow_hash_similarity, create_lookup


def compute_embedding(variable_name, m, local_word_emb):
    #  Map token indices to word embeddings
    var_bw = tt.lmatrix(variable_name + "_local_indx")

    def words_to_embs(w):
        return local_word_emb[w]

    btf, _ = theano.map(words_to_embs, [var_bw])
    btf = m.apply(btf)

    embs = btf.dimshuffle(1, 0, 2)

    return embs


def cnn_similarity(cnn_filters_nums, cnn_filter_lengths, si1_btf, si1_mask_bt, si2_btf, si2_mask_bt, symbols_num, embeddings_dim, embeddings):
    si1_bt = si1_btf[:, :, 0]
    si2_bt = si2_btf[:, :, 0]

    encoder = MultiConv1DMaxOverTime(filter_lengths=cnn_filter_lengths,
                                     filters_nums=cnn_filters_nums,
                                     input_dim=embeddings_dim,
                                     weights_init=IsotropicGaussian(0.1), biases_init=Constant(0), activation=Tanh())

    encoder.initialize()

    lookup = create_lookup(embeddings_dim, symbols_num, embeddings=embeddings)
    lookup.initialize()

    s1 = lookup.apply(si1_bt.T)
    ps1 = encoder.apply(s1, mask=si1_mask_bt.T)

    s2 = lookup.apply(si2_bt.T)
    ps2 = encoder.apply(s2, mask=si2_mask_bt.T)

    model = CosSim().apply(ps1, ps2)

    return model


def cbow_cnn_similarity(char_embeddings_dim, char_cnn_filters, char_cnn_filter_lengths,
                        si1_btf, si1_mask_bt, si2_btf, si2_mask_bt, symbols_num, embeddings_dim, embeddings=None):
    char_emb_dim = char_embeddings_dim
    max_char = 255
    character_lookup = LookupTable(max_char, char_emb_dim, name="char_lookup",
                                   weights_init=Uniform(width=0.2))
    character_lookup.initialize()

    char_conv = MultiConv1DMaxOverTime(filter_lengths=char_cnn_filter_lengths,
                                       filters_nums=char_cnn_filters,
                                       input_dim=char_emb_dim,
                                       weights_init=Uniform(width=.1),
                                       biases_init=Constant(0), activation=Rectifier())
    char_conv.initialize()

    m = Linear(sum(char_cnn_filters), embeddings_dim,
                    weights_init=Uniform(width=0.2),
                    biases_init=Constant(0),
                    use_bias=True,
                    name="transform"
                    )

    m.initialize()

    local_vocab_wc = tt.lmatrix("vocab")
    local_vocab_wc_mask = tt.matrix("vocab_mask")

    wcf = character_lookup.apply(local_vocab_wc)

    local_word_emb = char_conv.apply(wcf.dimshuffle(1, 0, 2), local_vocab_wc_mask.dimshuffle(1, 0))

    si1_tbf = compute_embedding("si1", m, local_word_emb)
    si2_tbf = compute_embedding("si2", m, local_word_emb)

    si1_btf = si1_tbf.dimshuffle(1, 0, 2) * si1_mask_bt[:, :, None]
    si2_btf = si2_tbf.dimshuffle(1, 0, 2) * si2_mask_bt[:, :, None]

    a = si1_btf.sum(axis=1)
    b = si2_btf.sum(axis=1)

    y_hat = (CosSim().apply(a, b) + 1)/2.0

    return y_hat
