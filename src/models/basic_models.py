import numpy
import theano
from blocks.bricks import Linear, Rectifier, Softmax, MLP, Identity, Logistic
from blocks.bricks import Tanh
from blocks.bricks.lookup import LookupTable
from blocks.bricks.recurrent import LSTM, SimpleRecurrent, Bidirectional, GatedRecurrent
from blocks.graph import apply_dropout, ComputationGraph
from blocks.initialization import Uniform, IsotropicGaussian, Constant
from theano import tensor
from theano.printing import Print

from custom_blocks.convolution.conv1d_max import MultiConv1DMaxOverTime
from custom_blocks.encoder import create_bidi_encoder
from custom_blocks.tools import CosSim


def create_lookup(embeddings_dim, symbols_num, embeddings=None):
    if embeddings is not None:
        symbols_num = embeddings.shape[0]
        if embeddings_dim != embeddings.shape[1]:
            raise ValueError
        print "Vocab size {}".format(symbols_num)
        lookup = LookupTable(symbols_num, embeddings_dim, name="lookup", weights_init=Constant(embeddings))
    else:
        # Lookup table with randomly initialized word embeddings
        lookup = LookupTable(symbols_num, embeddings_dim, name="lookup", weights_init=Uniform(width=0.2))

    return lookup

def cbow_hash_similarity(si1_btf, si1_mask_bt, si2_btf, si2_mask_bt, symbols_num, input_dim, embeddings_dim, embeddings=None):
    # networks parameters
    lookup = create_lookup(embeddings_dim, symbols_num, embeddings=embeddings)
    lookup.initialize()

    x_to_h = Linear(name='x_to_h',
                    input_dim=input_dim,
                    output_dim=embeddings_dim, weights_init=IsotropicGaussian(0.01), use_bias=False)
    x_to_h.initialize()

    si1_btf = x_to_h.apply(si1_btf) * si1_mask_bt[:, :, None]
    si2_btf = x_to_h.apply(si2_btf) * si2_mask_bt[:, :, None]

    a = si1_btf.sum(axis=1)
    b = si2_btf.sum(axis=1)

    y_hat = CosSim().apply(a, b)

    return y_hat

def cbow_similarity(si1_btf, si1_mask_bt, si2_btf, si2_mask_bt, symbols_num, embeddings_dim, embeddings):
    si1_bt = si1_btf[:, :, 0]
    si2_bt = si2_btf[:, :, 0]

    # networks parameters
    lookup = create_lookup(embeddings_dim, symbols_num, embeddings=embeddings)
    lookup.initialize()

    si1_btf = lookup.apply(si1_bt) * si1_mask_bt[:, :, None]
    si2_btf = lookup.apply(si2_bt) * si2_mask_bt[:, :, None]

    a = si1_btf.sum(axis=1)
    b = si2_btf.sum(axis=1)

    y_hat = CosSim().apply(a, b)

    return y_hat

def cbow_similarity_emb_lin(si1_btf, si1_mask_bt, si2_btf, si2_mask_bt, symbols_num, embeddings_dim, embeddings):
    si1_bt = si1_btf[:, :, 0]
    si2_bt = si2_btf[:, :, 0]

    # networks parameters
    lookup = create_lookup(embeddings_dim, symbols_num, embeddings=embeddings)
    lookup.initialize()

    si1_btf = lookup.apply(si1_bt) * si1_mask_bt[:, :, None]
    si2_btf = lookup.apply(si2_bt) * si2_mask_bt[:, :, None]

    x_to_h = Linear(name='emb_lin',
                    input_dim=embeddings_dim,
                    output_dim=embeddings_dim, weights_init=IsotropicGaussian(0.01), use_bias=False)
    x_to_h.initialize()

    si1_btf = x_to_h.apply(si1_btf)
    si2_btf = x_to_h.apply(si2_btf)

    a = si1_btf.sum(axis=1)
    b = si2_btf.sum(axis=1)

    y_hat = CosSim().apply(a, b)

    return y_hat
