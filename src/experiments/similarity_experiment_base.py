import os

import numpy
from blocks.extensions import Printing
from blocks.extensions.monitoring import DataStreamMonitoring
from fuel.schemes import ShuffledExampleScheme, ConstantScheme
from fuel.streams import DataStream
from fuel.transformers import Batch, Padding
from nltk import OrderedDict
from theano import tensor as tt
from blocks.serialization import load

from custom_blocks.custom_cost import CorrelationCost
from custom_blocks.data_stream_monitoring import FileDataStreamMonitoring
from custom_blocks.multi_output_model import MultiOutputModel
from datasets.string2token import String2IndexesOOVs
from datasets.sts_dataset import STSDataset
from datasets.unpickable_indexable_dataset import UnpickableIndexableDataset
from experiments.learning_experiment_base import LearningExperimentBase, str2bool
from tools.embedding_source import GensimWordEmbeddingSource, GloveWordEmbeddingSource, DynamicWordEmbeddingsSource
from tools.text_preprocess import TextPreprocess
from tools.text_utilities import get_vocab, get_oov_stat


class SimilarityExperimentBase(LearningExperimentBase):
    def __init__(self):
        super(SimilarityExperimentBase, self).__init__()

        self.embeddings_dim = self.args.embeddings_dim
        self.vocab_len = self.args.vocab_len
        self.number_of_oov = self.args.number_of_oov

        self.text_preprocess = TextPreprocess(tokenize_type=self.args.tokenize_type,
                                              lower_case=self.args.lower_case,
                                              stem=self.args.stem,
                                              use_tokenizer=self.args.use_tokenizer,
                                              remove_stopwords=self.args.remove_stopwords,
                                              lemmatize=self.args.lemmatize)

    def add_command_line_args(self):
        super(SimilarityExperimentBase, self).add_command_line_args()

        self.parser.add_argument('-ed', '--embeddings_dim', type=int, default=300,
                                 help='dimensions of embeddings for words')

        self.parser.add_argument('-vl', '--vocab_len', type=int, default=-1,
                                 help='number of words in vocabulary')

        self.parser.add_argument('-n_oov', '--number_of_oov', type=int, default=25,
                                 help='total number of oov')

        self.parser.add_argument('-rl', '--remove_lookup', type=str2bool, default="false",
                                 help='remove parameters for lookup')

        # text preprocess arguments
        self.parser.add_argument('-tt', '--tokenize_type', type=str, default="word",
                                 help='tokenize by word or character [word, char]')

        self.parser.add_argument('-lc', '--lower_case', type=str2bool, default="true",
                                 help='lower casing text')

        self.parser.add_argument('-st', '--stem', type=str2bool, default="true",
                                 help='stemming words')

        self.parser.add_argument('-tk', '--use_tokenizer', type=str2bool, default="true",
                                 help='using treebank  word tokenizer')

        self.parser.add_argument('-rs', '--remove_stopwords', type=str2bool, default="true",
                                 help='removing stopwords')

        self.parser.add_argument('-lm', '--lemmatize', type=str2bool, default="false",
                                 help='using WordNet lemmatizer')

    def get_dataset(self, filenames, vocab):
        text_2_token = String2IndexesOOVs(dictionary=vocab, preprocess=self.text_preprocess.preprocess,
                                          num_oov=self.number_of_oov)
        dataset = STSDataset(filenames, text_2_token)

        self.dataset = dataset
        return self.dataset

    def get_stream(self, filenames, batch_size=None, vocab=None, shuffle=False):
        if vocab is None:
            vocab = get_vocab(filenames, vocab_len=self.vocab_len,
                              sentence_to_tokens_fn=self.text_preprocess.preprocess_tokens, num_oov=self.number_of_oov)
        else:
            num_oov = get_oov_stat(vocab, filenames, vocab_len=self.vocab_len,
                                   sentence_to_tokens_fn=self.text_preprocess.preprocess_tokens,
                                   num_oov=self.number_of_oov)
            print "{} OOV in {}".format(num_oov, " ".join(filenames))

        dataset = self.get_dataset(filenames, vocab)
        stream = DataStream(dataset)

        # Load all data into memory, this way we avoid reloading the data from disk in every epoch
        memory_data = [[] for _ in dataset.sources]
        data_len = 0
        for ex in stream.get_epoch_iterator():
            data_len += 1
            for source_example, data_list in zip(ex, memory_data):
                data_list.append(source_example)

        data_dict = OrderedDict(zip(dataset.sources, memory_data))
        mem_dataset = UnpickableIndexableDataset(data_dict)

        if shuffle:
            # shuffle the data after each epoch of training
            pass
            #mem_dataset.example_iteration_scheme = ShuffledExampleScheme(mem_dataset.num_examples)
        stream = mem_dataset.get_example_stream()

        if batch_size is None:
            batch_size = data_len

        stream = Batch(stream, iteration_scheme=ConstantScheme(batch_size))
        stream = Padding(stream, mask_sources=['si1', 'si2'])

        return vocab, stream

    def get_sym_variables(self):
        si1_bt = tt.ltensor3('si1')
        si2_bt = tt.ltensor3('si2')

        si1_mask_bt = tt.matrix('si1_mask')
        si2_mask_bt = tt.matrix('si2_mask')
        baseline = tt.vector('baseline')[:, None]
        similarity = tt.vector('similarity')
        y = similarity[:, None]

        return si1_bt, si1_mask_bt, si2_bt, si2_mask_bt, y, baseline, similarity

    def load_embeddings(self, code2word, external_embedding_source):
        print "Loading precomputed embeddings ..."

        print "Embeddings loaded."
        word_embeddings_source = DynamicWordEmbeddingsSource(parent_word_embeddings_source=external_embedding_source,
                                                             verbose=True)
        # init embeddings
        for word in code2word:
            word_embeddings_source.get_word_index(word)

        word_embeddings_source.print_stats()

        embeddings_matrix = word_embeddings_source.get_word_embeddings_matrix()

        return embeddings_matrix

    def load_external_embeddings(self, vocab):
        # load pre-trained embeddings
        external_embedding_source = None
        if self.args.w2v_embeddings:
            external_embedding_source = GensimWordEmbeddingSource(self.args.w2v_embeddings)
        elif self.args.glove_embeddings:
            external_embedding_source = GloveWordEmbeddingSource(self.args.glove_embeddings)

        if external_embedding_source:
            code2token = [0] * len(vocab)
            for word, index in vocab.items():
                code2token[index] = word
            embeddings = self.load_embeddings(code2token, external_embedding_source)
        else:
            embeddings = None

        return embeddings

    def correlation_and_accuracy(self, y, y_hat):
        cost = -CorrelationCost(name="correlation").apply(y, y_hat)
        # cost = SquaredError().apply(y, y_hat)
        cost.name = "correlation"

        # back to 0 ... 5 TODO: better accuracy
        accuracy = tt.eq(tt.round(5 * y), tt.round(5 * y_hat)).mean()
        accuracy.name = "cor_accuracy"

        return cost, accuracy


    def init_model(self, vocab):
        embeddings = self.load_external_embeddings(vocab)

        si1_btf, si1_mask_bt, si2_btf, si2_mask_bt, y, baseline, similarity = self.get_sym_variables()

        y_hat = self.compute_prediction(si1_btf=si1_btf, si1_mask_bt=si1_mask_bt,
                                        si2_btf=si2_btf, si2_mask_bt=si2_mask_bt,
                                        symbols_num=len(vocab), embeddings_dim=self.embeddings_dim,
                                        embeddings=embeddings)
        y_hat.name = "y_hat"

        cost, accuracy = self.correlation_and_accuracy(y, y_hat)
        # baseline
        # cost, accuracy = self.correlation_and_accuracy(y, y_hat * 0 + baseline)


        model_outputs = (cost, accuracy, si1_btf, si1_mask_bt, si2_btf, si2_mask_bt, y, baseline, y_hat, similarity)

        model = MultiOutputModel(model_outputs)
        # store vocabulary as part of the model, so we can easily reuse it after unpickling
        model.vocabulary = vocab

        return cost, accuracy, model

    def export_embeddings(self, output_file, vocav, emb):
        with open(output_file, "w") as fo:
            for word, vector in zip(vocav, emb):
                vector_list = numpy.array(vector).tolist()
                fo.write("{} {}\n".format(word, " ".join([str(v) for v in vector_list])))


    def load_model(self):
        if self.args.load_model:
            # this code is executed only when the user loads already trained model
            with open(self.args.load_model, "rb") as model_file:
                print "Loading model {} ...".format(self.args.load_model)
                main_loop = load(model_file)  # load function from blocks.serialization
                model = main_loop.model

                if self.args.export_embeddings:
                    for parameter in model.parameters:
                        if parameter.tag.annotations[0].name == "lookup":
                            emb = parameter.get_value()

                    self.export_embeddings("test.txt", model.vocabulary, emb)

                cost, accuracy, si1_btf, si1_mask_bt, si2_btf, si2_mask_bt, y, baseline, y_hat, similarity = model.orig_outputs

                for test_file in self.args.test_files:
                    test_file_path = self.get_data_path(test_file)

                    # loop over multiple files that can be evaluated by the model
                    vocab, stream = self.get_stream(filenames=[test_file_path], vocab=model.vocabulary)
                    y_hat.name = "y_hat"
                    # use only one extension that will visualize the datastream

                    sys = y_hat[:, 0]
                    sys.name = "sys"

                    gs = y[:, 0]
                    gs.name = "gs"

                    output_directory = os.path.join(self.args.output_test, test_file)

                    stream_monitor = FileDataStreamMonitoring(output_directory=output_directory, variables=[gs, sys, cost],
                                                              data_stream=stream, before_training=True)

                    stream_monitor.main_loop = main_loop

                    main_loop.extensions = [stream_monitor]

                    main_loop._run_extensions('before_training')

                exit(0)

    def execute(self):
        self.load_model()

        train_file_paths = []
        vocab_file_paths = []
        for train_file in self.args.train:
            train_file_paths += [self.get_data_path(train_file)]
            vocab_file_paths += [self.get_data_path(train_file)]

        if self.args.glove_embeddings is not None or self.args.w2v_embeddings is not None:
            print "vocab also from train files"
            for test_file in self.args.test_files:
                vocab_file_paths += [self.get_data_path(test_file)]

        # vocab for embeddings may be also from test and valid files
        vocab, _ = self.get_stream(filenames=vocab_file_paths, vocab=None, batch_size=self.args.batch_size,
                                   shuffle=True)

        vocab, train_stream = self.get_stream(filenames=train_file_paths, vocab=vocab, batch_size=self.args.batch_size,
                                              shuffle=True)
        vocab, valid_stream = self.get_stream(filenames=[self.get_data_path(self.args.valid)], vocab=vocab)

        test_streams = []
        for test_file in self.args.test_files:
            test_file_path = self.get_data_path(test_file)
            vocab, test_stream = self.get_stream(filenames=[test_file_path], vocab=vocab)
            test_streams.append((test_stream, os.path.basename(test_file_path)))

        cost, accuracy, model = self.init_model(vocab)

        self.train(cost, accuracy, train_stream=train_stream, valid_stream=valid_stream, test_streams=test_streams,
                   model=model)
