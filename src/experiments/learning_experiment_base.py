import argparse
import logging
import os
import socket
import sys

from blocks.algorithms import AdaDelta, GradientDescent
from blocks.extensions import FinishAfter, Timing, Printing
from blocks.extensions.monitoring import TrainingDataMonitoring, DataStreamMonitoring
from blocks.extensions.saveload import Checkpoint
from blocks.extensions.stopping import FinishIfNoImprovementAfter
from blocks.extensions.training import TrackTheBest
from blocks.graph import ComputationGraph
from blocks.main_loop import MainLoop
from blocks.model import Model


def str2bool(v):
    return v.lower() in ("yes", "true", "t", "1")


def comma_sep_list(input_str):
    return input_str.split(",")


def comma_sep_int_list(input_str):
    if input_str == "None":
        return None
    else:
        return map(int, comma_sep_list(input_str))


def training_progress(progress):
    """
    Translates a string in format {x}, {x}E or {x}B to a Blocks extension calling frequency
    :param progress:
    :return:
    """
    progress = progress.lower()
    if progress.endswith('e'):
        return {"every_n_epochs": int(progress[:-1])}
    elif progress.endswith('b'):
        return {"every_n_batches": int(progress[:-1])}
    else:
        return {"every_n_epochs": int(progress)}


class LearningExperimentBase(object):
    """
    Base class for training models based on Blocks. It wraps functionality provided by MainLoop and its extensions from Blocks.
    """

    def __init__(self, description="NA", epilog="NA"):
        self.parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter,
                                              description=description, epilog=epilog)
        self.args = self.get_command_line_args()
        self.print_args()

        self.logger = logging.getLogger(__name__)
        print "Running on " + socket.gethostname()

    def print_args(self):
        print "Arguments: "
        print self.args

    def add_command_line_args(self):
        None

    def get_command_line_args(self):

        # parse commandline args
        self.parser.add_argument('--train', type=str, nargs='+', default=["2015.val.tsv"],
                                 help='train file')

        self.parser.add_argument('--valid', type=str, default="2015.val.tsv",
                                 help='valid file')

        self.parser.add_argument('--name', type=str, default="experiment",
                                 help='experiment name')

        self.parser.add_argument('--test_files', type=str, nargs='+', default=["2015.test.tsv"],
                                 help='test files')

        self.parser.add_argument('--dataset_root', type=str, default="../../data/semeval-sts/all/",
                                 help='root directory with dataset files')

        self.parser.add_argument('--load_model', type=str, default=None,
                            help='model to be loaded')

        self.parser.add_argument('--export_embeddings', action='store_true',
                            help='exporting embedding matrix')

        self.parser.add_argument('--output_test', type=str, default="./output/",
                                 help='output directory for testing loaded model')

        self.parser.add_argument('--epochs_max', type=int, default=20,
                                 help="maximum number of training epochs")

        self.parser.add_argument('-p', '--epochs_patience_valid', type=int, default=5,
                                 help="maximum number of training epochs without improvement in validation set accuracy"
                                      " before end of the training")

        self.parser.add_argument('--patience_metric', choices=['cost', 'accuracy'], default='cost',
                                 help="metric to monitor for early stopping")

        self.parser.add_argument('--disable_progress_bar', action='store_true',
                                 help="disables the progress bar that is shown during training, this might be handy "
                                      "when you want to store output of the training process in a file")

        self.parser.add_argument('--save_path', dest='save_path', default="model.blocks.save",
                            help='file where the saved model will be stored')

        self.parser.add_argument('--do_not_save', action='store_true',
                            help="do not save the model anytime, this option is usefull when one just wants to test convergence of the model")


        self.parser.add_argument('--save_every_n', type=training_progress, default="1",
                            help='frequency of saving the model during training')

        self.parser.add_argument('-b', '--batch_size', type=int, default="32",
                                 help='size of a mini-batch in gradient descend')

        self.parser.add_argument('-we', '--w2v_embeddings', type=str, default=None,
                                 help='file with binary Word2Vec representations. For example, you can use representations pretrained from Google News available on w2v homepage. ')

        self.parser.add_argument('-ge', '--glove_embeddings', type=str, default=None,
                                 help='file with binary Glove representations. For example, you can use representations pretrained on Wikipedia available on Glove homepage. ')

        self.add_command_line_args()

        args = self.parser.parse_args()

        return args

    def get_data_path_argparse(self, arg_name):
        file_path = getattr(self.args, arg_name)
        return self.get_data_path(file_path)

    def get_data_path(self, file_path):
        if os.path.isabs(file_path):
            return file_path
        else:
            if self.args.dataset_root:
                return os.path.join(self.args.dataset_root, file_path)
            else:
                return file_path

    @staticmethod
    def print_parameters_info(params_list):
        print "Parameters: "
        for parameter in params_list:
            print "\t" + str(parameter.tag.annotations[0].name) + "." + str(parameter) + " " + str(
                parameter.container.data.shape) + ".size=" + str(
                parameter.container.data.size)
        print "Trained parameters count: " + str(sum([parameter.container.data.size for parameter in params_list]))

    @staticmethod
    def data_monitoring_extensions(cost, accuracy, valid_stream, test_streams):
        extensions = list()

        # add extensions that monitors progress of training on train set
        extensions.append(TrainingDataMonitoring([cost, accuracy], after_epoch=True))

        extensions.append(Timing())
        extensions.append(
            DataStreamMonitoring([cost, accuracy], data_stream=valid_stream, prefix="valid", after_epoch=True))

        for test_stream, test_file_name in test_streams:
            extensions.append(
                DataStreamMonitoring([cost, accuracy], data_stream=test_stream, prefix=test_file_name,
                                     after_epoch=True))

        extensions.append(Printing(after_epoch=True))
        return extensions

    def create_extensions(self, cost, accuracy, valid_stream, test_streams):
        # this variable aggregates all extensions executed periodically during training
        extensions = []

        if self.args.epochs_max:
            # finis training after fixed number of epochs
            extensions.append(FinishAfter(after_n_epochs=self.args.epochs_max))

        extensions.extend(self.data_monitoring_extensions(cost, accuracy, valid_stream, test_streams))

        extensions.append(TrackTheBest("valid_" + cost.name, choose_best=min))

        extensions.append(
            # "valid_cost_best_so_far" message will be entered to the main loop log by TrackTheBest extension
            FinishIfNoImprovementAfter("valid_" + cost.name + "_best_so_far", epochs=self.args.epochs_patience_valid)
        )

        if not self.args.do_not_save:
            extensions.append(Checkpoint(self.args.save_path + "-" + self.args.name, **self.args.save_every_n))

        return extensions

    def train(self, cost, accuracy, train_stream, valid_stream, test_streams, model):
        # use all parameters of the model for optimization
        cg = ComputationGraph(cost)
        params_to_optimize = cg.parameters

        if self.args.remove_lookup:
            for parameter in params_to_optimize:
                if parameter.tag.annotations[0].name == "lookup":
                    params_to_optimize.remove(parameter)

        self.print_parameters_info(params_to_optimize)

        extensions = self.create_extensions(cost, accuracy, valid_stream, test_streams)

        step_rule = AdaDelta()
        algorithm = GradientDescent(cost=cost,
                                    parameters=params_to_optimize,
                                    step_rule=step_rule,
                                    on_unused_sources="warn")

        main_loop = MainLoop(data_stream=train_stream,
                             model=model,
                             algorithm=algorithm,
                             extensions=extensions
                             )
        sys.setrecursionlimit(1000000)
        main_loop.run()
