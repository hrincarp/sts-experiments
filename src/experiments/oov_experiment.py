from fuel.transformers import Padding

from custom_blocks.char_padding import VocabFromChars
from datasets.string2token import String2IndexesHash, String2IndexesChar
from datasets.sts_dataset import STSDataset
from experiments.learning_experiment_base import comma_sep_int_list
from experiments.similarity_experiment_base import SimilarityExperimentBase
from models.basic_models import cbow_similarity, cbow_similarity_emb_lin, cbow_hash_similarity
from models.cnn_model import cbow_cnn_similarity

"""
In this experiments we will try to find out best method for dealing with out of vocabulary words
"""


class OOVExperiment(SimilarityExperimentBase):
    def __init__(self):
        super(OOVExperiment, self).__init__()

        self.hash_embeddings_dim = self.args.hash_embeddings_dim

        # charcnn
        self.char_embeddings_dim = self.args.char_embeddings_dim
        self.cnn_filters = self.args.cnn_filters
        self.cnn_filter_lengths = self.args.cnn_filter_lengths

    def add_command_line_args(self):
        super(OOVExperiment, self).add_command_line_args()

        self.parser.add_argument('-t_oov', '--type_oov', type=str, default="oov",
                                 help='type of oov experiment [oov, hash, cnn, emb-fix]')

        self.parser.add_argument('-hed', '--hash_embeddings_dim', type=int, default=2000,
                                 help='dimensions of embeddings for hashing words')

        # charcnn
        self.parser.add_argument('-ched', '--char_embeddings_dim', type=int, default=2,
                                 help='dimensions of embeddings for characters')

        self.parser.add_argument('--cnn_filters', default=[25, 50, 75, 100, 125, 150], type=comma_sep_int_list,
                                 help='number of convolutional filters')

        self.parser.add_argument('--cnn_filter_lengths', default=[1, 2, 3, 4, 5, 6],
                                 type=comma_sep_int_list,
                                 help='length of convolutional filters')

    def get_dataset(self, filenames, vocab):
        if self.args.type_oov == "oov" or self.args.type_oov == "emb-fix":
            return super(OOVExperiment, self).get_dataset(filenames, vocab)

        if self.args.type_oov == "hash":
            return self.get_char_hash_dataset(filenames)

        if self.args.type_oov == "cnn":
            return self.get_char_cnn_dataset(filenames)

    def get_char_hash_dataset(self, filenames):
        text_2_token = String2IndexesHash(emb_dim=self.hash_embeddings_dim,
                                          preprocess=self.text_preprocess.preprocess
                                          )
        dataset = STSDataset(filenames, text_2_token)

        return dataset

    def get_char_cnn_dataset(self, filenames):
        text_2_token = String2IndexesChar(emb_dim=self.hash_embeddings_dim,
                                          preprocess=self.text_preprocess.preprocess
                                          )
        dataset = STSDataset(filenames, text_2_token)

        return dataset

    def get_stream(self, **params):
        vocab, stream = super(OOVExperiment, self).get_stream(**params)

        if self.args.type_oov == "cnn":
            stream = VocabFromChars(stream, words_sources=['si1', 'si2'])
            stream = Padding(stream, mask_sources=['vocab', 'si1_local_indx', 'si2_local_indx'])

        return vocab, stream

    def compute_prediction(self, **params):
        if self.args.type_oov == "oov":
            return cbow_similarity(**params)

        if self.args.type_oov == "emb-fix":
            return cbow_similarity_emb_lin(**params)

        if self.args.type_oov == "hash":
            return cbow_hash_similarity(input_dim=self.hash_embeddings_dim, **params)

        if self.args.type_oov == "cnn":
            return cbow_cnn_similarity(char_embeddings_dim=self.char_embeddings_dim,
                                       char_cnn_filters=self.cnn_filters,
                                       char_cnn_filter_lengths=self.cnn_filter_lengths, **params)

if __name__ == "__main__":
    exp = OOVExperiment()
    exp.execute()
