from experiments.oov_experiment import OOVExperiment
from models.cnn_model import cnn_similarity
from models.comp_models import max_cbow_similarity, min_cbow_similarity, mean_cbow_similarity, \
    sum_cbow_similarity
from models.rnn_models import rnn_similarity, lstm_similarity, bilstm_similarity


class STSExperiment(OOVExperiment):
    def __init__(self):
        super(STSExperiment, self).__init__()

    def add_command_line_args(self):
        super(STSExperiment, self).add_command_line_args()

        self.parser.add_argument('-m', '--model', type=str, default="cnn",
                                 help='type of oov experiment [max, min, mean, sum, rnn, cnn, lstm, bilstm]')

    def compute_prediction(self, **params):
        if self.args.model == "max":
            return max_cbow_similarity(**params)
        elif self.args.model == "min":
            return min_cbow_similarity(**params)
        elif self.args.model == "mean":
            return mean_cbow_similarity(**params)
        elif self.args.model == "sum":
            return sum_cbow_similarity(**params)

        elif self.args.model == "rnn":
            return rnn_similarity(**params)
        elif self.args.model == "lstm":
            return lstm_similarity(**params)
        elif self.args.model == "bilstm":
            return bilstm_similarity(**params)

        elif self.args.model == "cnn":
            return cnn_similarity(cnn_filters_nums=self.cnn_filters,
                                  cnn_filter_lengths=self.cnn_filter_lengths,
                                  **params)

        return super(STSExperiment, self).compute_prediction(**params)


if __name__ == "__main__":
    exp = STSExperiment()
    exp.execute()
