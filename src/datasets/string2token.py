import numpy as np

from tools.hash_ngram import compute_hash_repr
from tools.text_utilities import create_unk_tokens


class String2Indexes(object):
    def __init__(self, preprocess=None):
        self.preprocess = preprocess

    def word2index(self, word):
        pass

    def translate_one_line(self, sentence):
        if self.preprocess is not None:
            sentence = self.preprocess(sentence)

        data = []
        for word in sentence.split():
            word_index = self.word2index(word)
            if word_index is not None:
                data.append(word_index)

        return data


class String2IndexesOOVs(String2Indexes):
    def __init__(self, dictionary, preprocess=None, num_oov=1):
        super(String2IndexesOOVs, self).__init__(preprocess)

        self.dictionary = dictionary
        self.unk_tokens = create_unk_tokens(num_oov)

        for unk_token in self.unk_tokens:
            if unk_token not in self.dictionary:
                raise ValueError(unk_token + " not in dictionary")

    def word2index(self, word):
        try:
            return [self.dictionary[word]]
        except KeyError:
            if len(self.unk_tokens) == 0:
                return None

            word2unk = hash(word) % len(self.unk_tokens)
            return [self.dictionary[self.unk_tokens[word2unk]]]


class String2IndexesHash(String2Indexes):
    def __init__(self, emb_dim=100, preprocess=None):
        self.emb_dim = emb_dim
        super(String2IndexesHash, self).__init__(preprocess)

    def word2index(self, word):
        return np.array(compute_hash_repr(word, emb_dim=self.emb_dim), dtype=int)


class String2IndexesChar(String2Indexes):
    def __init__(self, emb_dim, preprocess=None):
        self.emb_dim = emb_dim
        super(String2IndexesChar, self).__init__(preprocess)

    def word2index(self, word):
        return word
