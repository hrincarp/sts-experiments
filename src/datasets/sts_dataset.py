import codecs

import numpy as np
import theano
from fuel.datasets import Dataset
from picklable_itertools import iter_, chain

from tools.text_utilities import compute_cos_sim

floatX = theano.config.floatX


class STSDataset(Dataset):
    provides_sources = ('similarity', 'si1', 'si2', 'baseline')
    example_iteration_scheme = None

    def __init__(self, files, text_to_token):
        self.files = files
        self.text_to_token = text_to_token

        super(STSDataset, self).__init__()

    def open(self):
        return chain(*[iter_(codecs.open(f, 'r', encoding="ascii", errors="ignore")) for f in self.files])

    def get_data(self, state=None, request=None):
        if request is not None:
            raise ValueError
        while True:
            line = next(state).strip()
            try:
                parts = line.split("\t")
                # print parts

                if len(parts) <= 3:
                    # semeval
                    similarity = np.asarray(float(parts[0]) / 5.0, dtype=floatX)
                else:
                    # msr dataset
                    similarity = np.asarray(float(parts[0]), dtype=floatX)

                (s1, si1), (s2, si2) = [(sentence, self.text_to_token.translate_one_line(sentence)) for sentence in
                                        parts[-2:]]

                # baseline = 0
                baseline = np.asarray(compute_cos_sim(si1, si2), dtype=floatX)

                # if one or both sentences are empty
                if len(si1) == 0 or len(si2) == 0:
                    continue

                return similarity, si1, si2, baseline
            except ValueError as e:
                pass
