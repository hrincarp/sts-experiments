import sys

import os
import web
from threading import Lock
from blocks.serialization import load

from custom_blocks.data_stream_monitoring import FileDataStreamMonitoring
from experiments.sts_experiment import STSExperiment


class STSServer(STSExperiment):
    mutex = Lock()

    def server_load_model(self, model_file_name):
            # this code is executed only when the user loads already trained model
            with open(model_file_name, "rb") as model_file:
                print "Loading model {} ...".format(model_file_name)
                main_loop = load(model_file)  # load function from blocks.serialization

                self.loaded_main_loop = main_loop

    def server_eval_file(self, filename):
        model = self.loaded_main_loop.model
        cost, accuracy, si1_btf, si1_mask_bt, si2_btf, si2_mask_bt, y, baseline, y_hat, similarity = model.orig_outputs

        # loop over multiple files that can be evaluated by the model
        vocab, stream = self.get_stream(filenames=[filename], vocab=model.vocabulary)
        y_hat.name = "y_hat"
        # use only one extension that will visualize the datastream

        sys = y_hat[:, 0]
        sys.name = "sys"

        gs = y[:, 0]
        gs.name = "gs"

        output_directory = "./output/"

        stream_monitor = FileDataStreamMonitoring(output_directory=output_directory, variables=[gs, sys, cost],
                                                  data_stream=stream, before_training=True)

        stream_monitor.main_loop = self.loaded_main_loop

        self.loaded_main_loop.extensions = [stream_monitor]

        self.loaded_main_loop._run_extensions('before_training')



class LoadModel:
    def GET(self):
        global sts_server
        model_path = web.input()["path"]
        sts_server.server_load_model(model_path)

        return "Done"

class TestFile:
    def GET(self):
        global sts_server
        file_path = web.input()["path"]
        sts_server.server_eval_file(file_path)

        return "Done"

class Interactive:
    def GET(self):
        print web.input()["s1"]
        print web.input()["s2"]

        return "hello"


def run_server():
    urls = ('/interactive/', 'Interactive',
            '/test/', 'TestFile',
            '/loadmodel/', 'LoadModel',
            )
    web.config.debug = False
    app = web.application(urls, globals())

    args = sys.argv
    sys.argv = []
    app.run()
    sys.argv = args
    print 'READY'


if __name__ == "__main__":
    sts_server = STSServer()

    run_server()
