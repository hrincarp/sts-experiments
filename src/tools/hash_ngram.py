import numpy as np
from nltk.util import ngrams


def compute_hash_repr(word, emb_dim=200, max_grams=20):
    h_hash = lambda x: hash(x) % emb_dim  # 0 ... emb_dim -1
    s_hash = lambda x: 2 * (hash(x) % 2) - 1  # -1 or 1

    chrs = [ord(c) for c in word]
    hash_vector = np.zeros(emb_dim)

    for ngram in xrange(max_grams + 1):
        for n in ngrams(chrs, ngram):
            value = hash(n)
            hash_vector[h_hash(value)] += s_hash(value)

    return hash_vector
