import codecs
import sys
from collections import Counter

import numpy as np


def create_unk_tokens(n):
    return ["<UNK_{}>".format(i) for i in xrange(0, n)]


default_special_tokens = []


def get_vocab(filenames, vocab_len=-1, sentence_to_tokens_fn=None, num_oov=1, special_tokens=default_special_tokens):
    if sentence_to_tokens_fn is None:
        sentence_to_tokens_fn = lambda x: x.split()

    vocabulary = get_vocabulary(filenames, sentence_to_tokens_fn)

    vocabulary.update({candidate: sys.maxint for candidate in special_tokens})
    oov_tokens = create_unk_tokens(num_oov)
    vocabulary.update({candidate: sys.maxint for candidate in oov_tokens})

    code2token = map(lambda x: x[0], vocabulary.most_common()[:len(special_tokens) + num_oov] + vocabulary.most_common()[
                                                                                      len(special_tokens) + num_oov:vocab_len])

    vocab = compute_token2code(code2token)

    return vocab


def get_oov_stat(old_vocab, filenames, vocab_len=-1, sentence_to_tokens_fn=None, num_oov=1,
                 special_tokens=default_special_tokens):
    vocab = get_vocab(filenames, vocab_len=vocab_len, sentence_to_tokens_fn=sentence_to_tokens_fn, num_oov=num_oov,
                      special_tokens=special_tokens)

    oov = set(vocab.keys()).difference(set(old_vocab.keys()))
    return len(oov)


def get_vocabulary(filenames, sentence_to_tokens_fn=None, progress_indication_lines=100000, print_stat=True,
                   skip_empty_sts=True, replace_tabs=True):
    word_counts = Counter()

    if not sentence_to_tokens_fn:
        sentence_to_tokens_fn = lambda x: x.split()

    line_counter = 0

    for filename in filenames:
        with codecs.open(filename, "r", "ascii", "ignore") as input_stream:
            for line in input_stream:
                # line = line.encode("utf-8").line.decode("ascii", "ignore")
                if skip_empty_sts:
                    if line.split("\t")[0].strip() == "":
                        continue

                if replace_tabs:
                    line = line.split("\t")[1] + " " + line.split("\t")[2]


                tokens = sentence_to_tokens_fn(line.rstrip('\n'))
                word_counts.update(tokens)

                line_counter += 1
                if line_counter % progress_indication_lines == 0 and print_stat:
                    print "Processed line " + str(line_counter)

            # summary statistics
            total_words = sum(word_counts.values())
            distinct_words = len(list(word_counts))

            if print_stat:
                print "STATISTICS {}".format(filename)
                print "Total words: " + str(total_words)
                print "Total distinct words: " + str(distinct_words)

    return word_counts


def compute_token2code(code2word):
    return {v: i for i, v in enumerate(code2word)}


def compute_cos_sim(set0, set1):
    set0 = set([x[0] for x in list(set0)])
    set1 = set([x[0] for x in list(set1)])

    p = (np.sqrt(len(set(set0)) * len(set(set1))))
    if p == 0:
        return 0

    return len(set(set0).intersection(set(set1))) / p
