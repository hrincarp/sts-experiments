from nltk import SnowballStemmer, word_tokenize
from nltk.corpus import stopwords
from nltk.stem import WordNetLemmatizer

class TextPreprocess:
    def __init__(self, tokenize_type="word", lower_case=True, stem=True, use_tokenizer=True, remove_stopwords=True, lemmatize=False):
        self.tokenize_type = tokenize_type
        self.lower_case = lower_case
        self.stem = stem
        self.use_tokenizer = use_tokenizer
        self.remove_stopwords = remove_stopwords
        self.lemmatize = lemmatize

        self.stopwords = set(stopwords.words('english')) if self.remove_stopwords else []

        if stem:
            self.stemmer = SnowballStemmer(language="english")

        if lemmatize:
            self.lemmatizer = WordNetLemmatizer()

    def tokenize(self, sentence):
        if self.tokenize_type == "word":
            return word_tokenize(sentence) if self.use_tokenizer else sentence.split()
        else:
            return sentence

    def preprocess_tokens(self, sentence):
        if self.lower_case:
            sentence = sentence.lower()

        def preprocess_word(word):
            if self.lemmatize:
                word = self.lemmatizer.lemmatize(word)

            if self.stem:
                word = self.stemmer.stem(word)

            return word

        tokens = []
        for word in self.tokenize(sentence):
            if not self.remove_stopwords or word.lower() not in self.stopwords:
                tokens.append(preprocess_word(word))

        return tokens

    def preprocess(self, sentence):
        return " ".join(self.preprocess_tokens(sentence))
