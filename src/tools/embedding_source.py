import gensim
import numpy as np
import sys


class WordEmbeddingSource(object):
    """
    Encapsulates word embeddings matrix and word2index dictionary.
    User has to make sure that unknown_token is present in the vocabulary.
    """

    def __init__(self, embeddings=None, word2index={}, unknown_token='<UNK>'):
        self.embeddings = embeddings
        self.word2index = word2index
        self.unknown_token = unknown_token

    def get_word_index(self, word):
        """
        Get index for word. Possibly adding the word to a list.
        :param word:
        :return:
        """
        if word not in self.word2index:
            return None
        else:
            return self.word2index[word]

    def get_word_embeddings_matrix(self):
        return self.embeddings


class GloveWordEmbeddingSource(WordEmbeddingSource):
    """
    Word embeddings initialized from file.
    """

    def __init__(self, embeddings_filename):
        self.embeddings_filename = embeddings_filename
        self.load_embeddings_from_file()

    def load_embeddings_from_file(self):
        # load embeddings from file
        word2wordvector = self.load_glove_vec(
                self.embeddings_filename)
        # create dictionary mapping words to indices
        self.word2index = {word: index for (index, word) in enumerate(word2wordvector.keys())}

        # transform dictionary to Numpy matrix
        self.embeddings = np.array(
                list(word2wordvector.itervalues()))

    def load_glove_vec(self, fname, binary=False):
        """
        Loads word vecs from gloVe
        code from https://github.com/npow/ubottu/blob/master/src/merge_data.py#L73
        """
        word_vecs = {}
        mode = "rb" if binary else "r"
        with open(fname, mode) as f:
            for i, line in enumerate(f):
                L = line.split()
                word = L[0].lower()
                # if word in vocab:
                word_vecs[word] = np.array(L[1:], dtype='float32')
        return word_vecs

    def __getstate__(self):
        # remove word embeddings since we do not want to save them, they are already in a file
        out_dict = self.__dict__.copy()
        del out_dict['embeddings']
        del out_dict['word2index']
        return out_dict

    def __setstate__(self, dict):
        self.__dict__.update(dict)
        self.load_embeddings_from_file()
        return self.__dict__


class GensimWordEmbeddingSource(WordEmbeddingSource):
    """
    Word embeddings initialized from file.
    """

    def __init__(self, embeddings_filename):
        self.embeddings_filename = embeddings_filename
        self.binary = embeddings_filename.split(".")[-1] != "txt"
        self.load_embeddings_from_file()

    def load_embeddings_from_file(self):
        # load embeddings from file
        word2wordvector = gensim.models.Word2Vec.load_word2vec_format(self.embeddings_filename, binary=self.binary)


        # create dictionary mapping words to indices
        self.word2index = {word: index for (index, word) in enumerate(word2wordvector.index2word)}

        # transform dictionary to Numpy matrix
        self.embeddings = np.array(map(lambda word: word2wordvector[word], word2wordvector.index2word))

    def __getstate__(self):
        # remove word embeddings since we do not want to save them, they are already in a file
        out_dict = self.__dict__.copy()
        del out_dict['embeddings']
        del out_dict['word2index']
        return out_dict

    def __setstate__(self, dict):
        self.__dict__.update(dict)
        self.load_embeddings_from_file()
        return self.__dict__


class DynamicWordEmbeddingsSource(WordEmbeddingSource):
    """
    Source that can dynamically add new word vectors to its internal dictionary.
    The default algorithm is following:
        1. if the word already exists return it
        2. otherwise ask parent_word_embeddings_source and add the word when it exists there
        3. otherwise generate random word embedding when it is appropriate TODO
    """

    def __init__(self, parent_word_embeddings_source, verbose=False):
        self.parent_word_embeddings_source = parent_word_embeddings_source
        # when true then only words from the parent can be added, however, randomly initialized words will not be created
        self.allow_to_add_new_words = True
        self.vectors_to_be_added = []
        self.parent_vectors_std = None
        self.verbose = verbose
        self.stat_words_random_init = 0
        self.stat_words_parent_init = 0
        super(DynamicWordEmbeddingsSource, self).__init__()

    def print_stats(self):
        print "Words initialized from parent: " + str(self.stat_words_parent_init)
        print "Words initialized randomly: " + str(self.stat_words_random_init)

    def get_word_index(self, word):
        """
        Get index for word. Possibly adding the word to a list.
        :param word:
        :return:
        """
        if word in self.word2index:
            # the word is already stored, return its index
            return self.word2index[word]
        else:
            # try to get the word from parent
            parentWordIx = self.parent_word_embeddings_source.get_word_index(word)
            newWordIx = len(self.word2index)
            if parentWordIx is not None:
                newWordEmbedding = self.parent_word_embeddings_source.get_word_embeddings_matrix()[parentWordIx]
                self.stat_words_parent_init += 1

            elif self.allow_to_add_new_words:
                parent_embeddings = self.parent_word_embeddings_source.get_word_embeddings_matrix()

                word_dims = len(parent_embeddings[0])

                if not self.parent_vectors_std:
                    print "Computing std of parent vectors ..."
                    self.parent_vectors_std = np.std(parent_embeddings)
                    print "std=" + str(self.parent_vectors_std)

                mean = 0

                newWordEmbedding = mean + self.parent_vectors_std * np.random.randn(1, word_dims)

                self.stat_words_random_init += 1
            else:
                return None

            # add the word mapping to the dictionary
            self.word2index[word] = newWordIx

            # add the word embedding

            self.vectors_to_be_added.append(newWordEmbedding)

            return newWordIx

        return None

    def get_new_words_embeddings_matrix(self):
        # get only new vectors added after calling get_word_embeddings_matrix
        if self.vectors_to_be_added:
            to_stack = []
            to_stack.extend(self.vectors_to_be_added)
            self.vectors_to_be_added = []
            new_embeddings = np.vstack(to_stack)
            return new_embeddings

        return None

    def get_word_embeddings_matrix(self):
        if self.vectors_to_be_added:
            # there are some cached vectors that were not added to the embeddings matrix, add them
            to_stack = []
            if self.embeddings is not None:
                to_stack.append(self.embeddings)

            to_stack.extend(self.vectors_to_be_added)
            self.vectors_to_be_added = []

            self.embeddings = np.vstack(to_stack)

        return self.embeddings
