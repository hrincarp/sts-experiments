from collections import defaultdict

import argparse
import re
from itertools import cycle

import matplotlib.pyplot as plt

"""
Draws a graph from Blocks training text log (printed to stdout).

"""

parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter,
                                 description="Utility that plots training progress stored in stdout output of Blocks training log.")

parser.add_argument('-m','--metric_to_plot', nargs='+', default="valid_cost",
                    help='name of a metrics to plot')

parser.add_argument('-f', '--file', nargs='+',
                    help='log files to plot in a graph')

parser.add_argument('-p', '--path', action='store_true',
                    help='use full path of log files')

#parser.add_argument('-b', '--batches', action='store_true',
#                    help='use number of batches for X axis')

args = parser.parse_args()


def load_text_file(filename, logged_value):
    """
    Loads values from a single training log.
    :param filename:
    :param logged_value:
    :return:
    """

    # get batch size
    pattern_batch = re.compile(".*-b_([0-9]*).*")
    m = pattern_batch.match(filename)
    if m:
        batch_size = int(m.group(1))
    else:
        batch_size = 32

    pattern_iter = re.compile(".* iterations_done: (.*)")
    iters = 0

    metric_to_values = {}
    pattern = re.compile(".* (" + logged_value + "): (.*)")
    end_pattern = re.compile("Sender: LSF System.*")

    with open(filename) as f:
        for line in f:
            end = end_pattern.match(line)
            if end: break
            m = pattern.match(line)
            if m:
                # pick metric name
                metric_name = m.group(1)
                if metric_name in metric_to_values:
                    values = metric_to_values[metric_name]
                else:
                    values = []
                    metric_to_values[metric_name] = values

                # pick the value from regexp
                values.append((iters*batch_size,float(m.group(2))))
            else:
                m = pattern_iter.match(line)
                if m:
                  iters = int(m.group(1))

    #print (values)
    return metric_to_values



def load_multiple_values_from_multiple_files(files_list, logged_values):
    metric_to_file_to_values = defaultdict(dict)

    out = dict()
    for logged_value in logged_values:
        file_to_metric_to_vals = load_values_from_multiple_files(files_list, logged_value)
        for file, metric_to_vals in file_to_metric_to_vals.iteritems():
            for metric, vals in metric_to_vals.iteritems():
                metric_to_file_to_values[metric][file] = vals


        #out.update(loaded_values)
    # strip dirs in filepaths
    return metric_to_file_to_values


def get_or_create(dic, key):
    if key in dic:
        return dic[key]
    else:
        val = []
        dict[key] = val
        return val


def find_max_in_dict_of_dicts(dict_of_dicts_of_lists):
    metric_to_file_max_tuple = defaultdict(list)

    for file, metric_to_vals in dict_of_dicts_of_lists.iteritems():
        for metric, values in metric_to_vals.iteritems():
            ticks, vals = zip(*values)
            maximum = max([0] + list(vals))
            metric_to_file_max_tuple[metric].append((file, maximum))

    # find absolute max
    for metric, file_to_max in metric_to_file_max_tuple.iteritems():
        file_and_max = sorted(file_to_max, key=lambda x: x[1])
        print
        print "{} best results".format(metric)
        for i,pair in enumerate(file_and_max):
            print "{}. {}".format(i, pair)


def load_values_from_multiple_files(files_list, logged_value):
    loaded_values = map(lambda x: load_text_file(x, logged_value), files_list)
    # strip dirs in filepaths
    if args.path:
        filenames = files_list
    else:
        filenames = [x.split("/")[-1] for x in files_list]

    lines_dict = dict(zip(filenames, loaded_values))

    # print table sorted by extreme values
    find_max_in_dict_of_dicts(lines_dict)

    return lines_dict


lines_dict = load_multiple_values_from_multiple_files(args.file, args.metric_to_plot)


fig = plt.figure()


# line stzles
lines = ["-","--","-."]
linecycler = cycle(lines)


for metric, lines in lines_dict.iteritems():
    # add all lines to a plot
    style = next(linecycler)
    for name, sequence in lines.iteritems():
        ticks, vals = zip(*sequence)

        plt.plot(ticks, vals, label=metric+"_"+name, picker=3, linestyle=style)


# mouse hover event handling function
def onpick(event):
    thisline = event.artist
    xdata = thisline.get_xdata()
    ydata = thisline.get_ydata()
    ind = event.ind
    print "Label: " + thisline._label
    print 'onpick points:', zip(xdata[ind], ydata[ind])

fig.canvas.mpl_connect('pick_event', onpick)


lgd = plt.legend(bbox_to_anchor=(0.5, -0.1), loc=9, borderaxespad=0.)
#fig.savefig('samplefig.png', bbox_extra_artists=(lgd,), bbox_inches='tight')

plt.show()
