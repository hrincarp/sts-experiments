"""
This scripts generates table for text preprecessing and number of oov
"""
import glob
import os

import re

from scripts.parse_results import load_text_file


def row_for_file(filename):
    with open(filename) as f:
        all_text = f.read()

    pattern = r".*\n([0-9]+) OOV in .*test.*$"

    # get batch size
    pattern_batch = re.compile(pattern, re.DOTALL)
    m = pattern_batch.match(all_text)
    if m:
        oov = int(m.group(1))
    else:
        raise ValueError

    metric_to_values = load_text_file(all_text, ".*test.*correlation.*")

    accuracy = metric_to_values.values()[0][-1][-1]


    print r"{} & {:.2f} & {} \\ \hline ".format(os.path.basename(filename).split("_")[1], -accuracy, oov)

experiments_path = "/Users/theses/Developer/sts-experiments/experiments/output/oov_text_preprocess/*"
files = glob.glob(experiments_path)

for file in files:
    row_for_file(file)

# all & 0.83 & 3762 \\ \hline
# lowercase & 0.80 & 8407 \\ \hline
# none & 0.77 & 9945 \\ \hline
# stemming & 0.81 & 6634 \\ \hline
# stopwords & 0.78 & 9881 \\ \hline
# tokenizer & 0.79 & 7082 \\ \hline
