"""
This scripts generates table for using external embeddings
"""
import glob
import os

import re

from scripts.parse_results import load_text_file


def row_for_file(filename):
    with open(filename) as f:
        all_text = f.read()

    pattern = r".*\n([0-9]+) OOV in .*test.*$"

    # get batch size
    pattern_batch = re.compile(pattern, re.DOTALL)
    m = pattern_batch.match(all_text)
    if m:
        oov = int(m.group(1))
    else:
        raise ValueError

    metric_to_values = load_text_file(all_text, ".*test.*correlation.*")

    accuracy = metric_to_values.values()[0][-1][-1]
    accuracy_0 = metric_to_values.values()[0][0][-1]


    print r"{} & {:.3f} & {:.3f} \\ \hline ".format(os.path.basename(filename).split("_")[1:3],  -accuracy_0, -accuracy)

experiments_path = "/Users/theses/Developer/sts-experiments/experiments/output/oov_external_emb/*"
files = glob.glob(experiments_path)

for file in files:
    row_for_file(file)

