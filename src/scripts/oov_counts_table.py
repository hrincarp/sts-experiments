"""
This scripts generates table for number of oov tokens
"""
import glob
import os

import re

from scripts.parse_results import load_text_file


def row_for_file(filename):
    with open(filename) as f:
        all_text = f.read()

    pattern = r".*\n([0-9]+) OOV in .*test.*$"

    # get batch size
    pattern_batch = re.compile(pattern, re.DOTALL)
    m = pattern_batch.match(all_text)
    if m:
        oov = int(m.group(1))
    else:
        raise ValueError

    metric_to_values = load_text_file(all_text, ".*test.*correlation.*")

    accuracy = metric_to_values.values()[0][-1][-1]


    print r"{} & {:.3f} \\ \hline ".format(os.path.basename(filename).split("_")[1], -accuracy)

experiments_path = "/Users/theses/Developer/sts-experiments/experiments/output/oov_counts/*"
files = glob.glob(experiments_path)

for file in files:
    row_for_file(file)


# oov0 & 0.833 \\ \hline
# oov1 & 0.829 \\ \hline
# oov1l & 0.821 \\ \hline
# oov1ll & 0.818 \\ \hline
# oov25 & 0.858 \\ \hline
# oov25l & 0.857 \\ \hline
# oov25ll & 0.856 \\ \hline
# oov2 & 0.853 \\ \hline
# oov2l & 0.847 \\ \hline
# oov2ll & 0.838 \\ \hline
# oov5 & 0.856 \\ \hline
# oov5l & 0.856 \\ \hline
# oov5ll & 0.848 \\ \hline
