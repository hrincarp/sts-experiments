import re


def load_text_file(text, logged_value):
    """
    Loads values from a single training log.
    :param filename:
    :param logged_value:
    :return:
    """

    # get batch size
    pattern_batch = re.compile(".*?batch_size=([0-9]*).*?", re.DOTALL)
    m = pattern_batch.match(text)
    if m:
        batch_size = int(m.group(1))
    else:
        raise ValueError

    pattern_iter = re.compile(".* iterations_done: (.*)")
    iters = 0

    metric_to_values = {}
    r = r".*(" + logged_value + "): (.*)"
    pattern = re.compile(r)

    for line in text.split("\n"):
        m = pattern.match(line)
        if m:
            # pick metric name
            metric_name = m.group(1)
            if metric_name in metric_to_values:
                values = metric_to_values[metric_name]
            else:
                values = []
                metric_to_values[metric_name] = values

            # pick the value from regexp
            values.append((iters * batch_size, float(m.group(2))))
        else:
            m = pattern_iter.match(line)
            if m:
                iters = int(m.group(1))

    # print (values)
    return metric_to_values
