import codecs

from nltk import SnowballStemmer

from tools.text_preprocess import TextPreprocess
from tools.text_utilities import get_vocabulary, get_vocab

#input_word2vec = "/Users/theses/THESIS/PROGRAM/DATA/word2vec/vectors.txt"
input_word2vec = "/Users/theses/Developer/glove.6B/glove.6B.300d.txt"
#outout_word2vec = "/Users/theses/THESIS/PROGRAM/DATA/word2vec/vectors_semeval_new_stem.txt"
outout_word2vec = "/Users/theses/Developer/glove.6B/glove.6B.300d.semeval_new.txt"


filenames = [
    "/Users/theses/Developer/sts-experiments/data/semeval-sts/all/2015.train.tsv",
"/Users/theses/Developer/sts-experiments/data/semeval-sts/all/2015.test.tsv",
    "/Users/theses/Developer/sts-experiments/data/semeval-sts/all/2015.val.tsv"
]

text_preprocess = TextPreprocess(lower_case=True,
                                      stem=False,
                                      use_tokenizer=True,
                                      remove_stopwords=False,
                                      lemmatize=False)

vocabulary = get_vocab(filenames, sentence_to_tokens_fn=text_preprocess.preprocess_tokens)
vocabulary = vocabulary.keys()
print vocabulary

def filter():
    stemmer = SnowballStemmer(language="english")
    print "reading word2vec ..."

    f_out = open(outout_word2vec, 'w')
    f_in = codecs.open(input_word2vec, 'r', "ascii", "ignore")
    i = 0
    for l in f_in:
        i += 1
        #word = stemmer.stem(l.split()[0])
        word = l.split()[0]
        if word in vocabulary:
            f_out.write(l)
            vocabulary.remove(word)
            print i
            print len(vocabulary)

    f_out.close()
    f_in.close()


filter()
